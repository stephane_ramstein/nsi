#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Programme écrit par NOM PRENOM
Classe de NSI1 ou NSI2
'''


## Définition des fonctions

def nand(a, b):
    '''
    Fonction qui calcule la fonction NAND.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    s = not(a and b)
    return s

def fonction1(a):
    '''
    Fonction qui calcule la fonction 1.
    Entrée a : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    s = nand(a, a)
    return s

def fonction2(a, b):
    '''
    Fonction qui calcule la fonction 2.
    Entrée a : booléen, 0 ou 1, False ou True
    Entrée b : booléen, 0 ou 1, False ou True
    Sortie s : booléen, 0 ou 1, False ou True
    '''
    i1 = nand(a, b)
    s = nand(i1, i1)
    return s


## Programme principal

print(int(nand(0, 0)))
