## Python - Constructions élémentaires - Programmation procédurale

Le cours précédent est complété avec la programmation structurée et quelques aspects modulaires.

- TP-cours sur les fonctions : [pdf](./03_python_constructions_elementaires/Cours_Python_03_Fonctions.pdf) ou [odt](./03_python_constructions_elementaires/Cours_Python_03_Fonctions.odt)
- Compléments sur la notion de variables locales et globales : [pdf](./03_python_constructions_elementaires/Complements_fonctions_variables_locales_globales.pdf) ou [odt](./03_python_constructions_elementaires/Complements_fonctions_variables_locales_globales.odt)
- Compléments sur la création de modules : [pdf](./03_python_constructions_elementaires/Complements_creer_module.pdf) ou [odt](./03_python_constructions_elementaires/Complements_creer_module.odt)
- Fiche exercices sur les fonctions : [pdf](./03_python_constructions_elementaires/Exercices_Python_02_Fonctions_et_turtle.pdf) ou [odt](./03_python_constructions_elementaires/Exercices_Python_02_Fonctions_et_turtle.odt)


## Python - structures de données : les séquences - chaînes de caractères

- Cours d'introduction aux séquences en Python : [pdf](./04_python_sequences/Cours_Python_04a_Les_sequences_introduction.pdf) ou [odt](./04_python_sequences/Cours_Python_04a_Les_sequences_introduction.odt)
- TP-cours sur les chaînes de caractères : [pdf](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.pdf) ou [odt](./04_python_sequences/Cours_Python_04b_Les_sequences_chaines.odt)
- Compléments sur le formatage des chaînes de caractères : [pdf](./04_python_sequences/Complements_chaines.pdf) ou [odt](./04_python_sequences/Complements_chaines.odt)
- Fiche d'exercices pratiques Python sur les chaînes de caractères : [pdf](./04_python_sequences/Exercices_Python_04_chaines.pdf) ou [odt](./04_python_sequences/Exercices_Python_04_chaines.odt)
- Fiche d'exercices écrits Python sur les chaînes de caractères : [pdf](./04_python_sequences/exos_chaines.pdf) ou [odt](./04_python_sequences/exos_chaines.odt)


## Python - structures de données : les séquences - n-uplets

- TP-cours sur les n-uplets : [pdf](./04_python_sequences/Cours_Python_04c_Les_sequences_tuples.pdf) ou [odt](./04_python_sequences/Cours_Python_04c_Les_sequences_tuples.odt)
- Fiche d'exercices pratiques Python sur les n-uplets : [pdf](./04_python_sequences/exos_tuples.pdf) ou [odt](./04_python_sequences/exos_tuples.odt)


## Python - structures de données : les séquences - tableaux

- TP-cours sur les tableaux : [pdf](./04_python_sequences/Cours_Python_04d_Les_sequences_listes.pdf) ou [odt](./04_python_sequences/Cours_Python_04d_Les_sequences_listes.odt)


## Interaction Homme-Machine sur le web
- TP construction d'un site web statique intéractif : [pdf](./06_ihm_web/01_construction_site_web/construction_site_web.pdf) ou [odt](./06_ihm_web/01_construction_site_web/construction_site_web.odt)
    * Ressources pour le TP site web : [zip](./06_ihm_web/01_construction_site_web/images.zip)
<!-- - TP introduisant la notion de formulaire : [pdf](./06_ihm_web/04_javascript/TP_JavaScript.pdf)
    * Ressources pour le TP : [zip](./06_ihm_web/04_javascript/index.zip) -->
- Compléments sur la couleur en CSS : [pdf](./06_ihm_web/02_site_flexbox/couleurs_en_CSS.pdf) ou [odt](./06_ihm_web/02_site_flexbox/couleurs_en_CSS.odt)
- TP sur les formulaires : [pdf](./06_ihm_web/03_client_serveur/TP_Get_Post.pdf)
    - fichiers pour le TP : [zip](./06_ihm_web/03_client_serveur/TP_Get_Post.zip)

  <!-- - TP-cours introductif Internet, Web et site web : [pdf](./06_ihm_web/00_introduction_WEB/TP_web.pdf) ou [odt](./06_ihm_web/00_introduction_WEB/TP_web.odt)
    * Ressources pour le TP introductif : [zip](./06_ihm_web/00_introduction_WEB/tp_web.zip) -->


## Algorithmique - Parcours séquentiel d'un tableau

Un certain nombre d'algorithmes fondamentaux sont à maîtriser. Leur étude donnera une meilleure compréhension et maîtrise de la programmation. Ces algorithmes seront abordés au fur et à mesure des deux années de NSI.

- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)
<!-- - Travail pratique autour du calcul de moyenne, S -->


## Algorithmique - Complexité

<!-- faire le point sur les algorithmes à connaître par coeur -->

- Introduction à l'algorithmique : [pdf](./10_algorithmique/cours_algo_1_intro.pdf) ou [odt](./10_algorithmique/cours_algo_1_intro.odt)
- Complexité en temps : [pdf](./10_algorithmique/cours_algo_2_complexite.pdf) ou [odt](./10_algorithmique/cours_algo_2_complexite.odt)
- Correction d'un algorithme : [pdf](./10_algorithmique/cours_algo_3_correction.pdf) ou [odt](./10_algorithmique/cours_algo_3_correction.pdf)


## Traitement des données en table

Introduction au traitement de données en utilisant les tableaux (listes) à deux dimensions pour représenter les tables.

- TP-cours sur les fichiers CSV : [pdf](./09_traitement_donnees/ouverture_fichiers_csv.pdf) ou [odt](./09_traitement_donnees/ouverture_fichiers_csv.odt)
- Cours sur les données et leur structuration : [pdf](./09_traitement_donnees/cours_donnees_structuration.pdf) ou [odt](./09_traitement_donnees/cours_donnees_structuration.odt)
- Activités sur le traitement des données : [pdf](./09_traitement_donnees/traitement_donnees_table.pdf) ou [odt](./09_traitement_donnees/traitement_donnees_table.odt)
  * Ressources : [zip](./09_traitement_donnees/pokemon_stats.zip)


## Algorithmique - Les tris

Deux tris sont à l'étude. Lors du tri par sélection, à chaque étape, on sélectionne tout simplement le plus petit élément restant que l'on place à la suite dans la liste triée. Lors du tri par insertion, chaque élément est tour à tour inséré à sa place dans la liste triée.

- Illustration du tri par sélection : [pdf](./10_algorithmique/02_tris/SR_01_tri_selection.pdf) ou [odp](./10_algorithmique/02_tris/SR_01_tri_selection.odp)
- Illustration du tri par insertion : [pdf](./10_algorithmique/02_tris/SR_02_tri_insertion.pdf) ou [odp](./10_algorithmique/02_tris/SR_02_tri_insertion.odp)
- Travail pratique autour des tris : [md](./10_algorithmique/TP_algo_02_tris.md)


## Algorithmique - algorithmes gloutons (Cours de Philippe Boddaert)

- Diaporama de cours : [pdf](./10_algorithmique_philippe/glouton/diapo.pdf) ou [odp](./10_algorithmique_philippe/glouton/diapo.odp)
- TD - Problème du sac à dos : [pdf](./10_algorithmique_philippe/glouton/td/enonce_backpack.pdf) ou [odt](./10_algorithmique_philippe/glouton/td/enonce_backpack.odt)
- TD - Rendu de monnaie : [pdf](./10_algorithmique_philippe/glouton/td/enonce_rendu.pdf) ou [odt](./10_algorithmique_philippe/glouton/td/enonce_rendu.odt)
- Exercices : [pdf](./10_algorithmique_philippe/glouton/exercices/enonce.pdf) ou [odp](./10_algorithmique_philippe/glouton/exercices/enonce.odt)

## Algorithmique - Recherche dichotomique (Cours de Pilippe Boddaert)

- Diaporama de cours : [pdf](./10_algorithmique_philippe/dichotomie/diapo.pdf) ou [odp](./10_algorithmique_philippe/dichotomie/diapo.odp)
- TD - Implémentation : [pdf](./10_algorithmique_philippe/dichotomie/td/enonce.pdf) ou [odt](./10_algorithmique_philippe/dichotomie/td/enonce.odt)
- TP - Juste Prix : [pdf](./10_algorithmique_philippe/dichotomie/tp/enonce.pdf) ou [odt](./10_algorithmique_philippe/dichotomie/tp/enonce.odt)


## Architecture réseau : les bases

- Diapo cours sur l'adressage, le protocole TCP-IP, l'encapsulation des données et la topologie des réseaux : [pdf](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.pdf) ou [odp](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.odp)
  * Vidéo sur l'histoire d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/0_Historique_et_Principe.mp4)
  * Vidéo sur le fonctionnement d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/1_Internet_Comment_ca_marche.mp4)
  * Vidéo sur le protocole TCP-IP : [mp4](./07_architecture_reseau/01_cours_reseau/3_MOOC_SNT_Internet_Protocole_TCP_IP.mp4)
- Fiche sur les protocoles IP et TCP : [pdf](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf) ou [odt](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.odt)
- TP sur la construction et la configuration d'un réseau type internet : [pdf](./07_architecture_reseau/02_contruction_configuration_reseau/Filius_NSI.pdf)

- TP sur la construction d'un réseau avec serveur DNS : [pdf](./07_architecture_reseau/TP_DNS.pdf)


## Représentation des données : les caractères

- Illustrations de cours : [pdf](./05_representation_donnees/05_caracteres/illustrations_cours.pdf) ou [odp](./05_representation_donnees/05_caracteres/illustrations_cours.odp)
- Notions de cours et mise en pratique : [pdf](./05_representation_donnees/05_caracteres/representation_des_caracteres.pdf) ou [odt](./05_representation_donnees/05_caracteres/representation_des_caracteres.odt)
- Le code Baudot des Telex : [pdf](./05_representation_donnees/05_caracteres/TP_Baudot.pdf) ou [odt](./05_representation_donnees/05_caracteres/TP_Baudot.odt)
- exercice HTML et charset : [pdf](./05_representation_donnees/05_caracteres/HTML_charset.pdf) ou [odt](./05_representation_donnees/05_caracteres/HTML_charset.odt)
- Notions et activités autour des fichiers textes et de leur encodage : [pdf](./05_representation_donnees/05_caracteres/ouverture_fichiers_texte.pdf) ou [odt](./05_representation_donnees/05_caracteres/ouverture_fichiers_texte.odt)



## Algorithmique - algorithmes classiques à connaître

Un certains nombres d'algorithmes fondamentaux sont à maîtriser. Leur étude donnera une meilleure compréhension et maîtrise de la programmation. Ces algorithmes seront abordés au fur et à mesure des deux années de NSI.

- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)


## Algorithmique : vers la résolution des problèmes complexes

- Travail pratique autour de l'algorithme des plus proches voisins et les algorithmes gloutons : [md](./10_algorithmique/TP_algo_04_algorithmes_intelligents.md)


## Architecture réseau : les bases

- Diapo cours sur l'adressage, le protocole TCP-IP, l'encapsulation des données et la topologie des réseaux : [pdf](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.pdf) ou [odp](./07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.odp)
  * Vidéo sur l'histoire d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/0_Historique_et_Principe.mp4)
  * Vidéo sur le fonctionnement d'internet : [mp4](./07_architecture_reseau/01_cours_reseau/1_Internet_Comment_ca_marche.mp4)
  * Vidéo sur le protocole TCP-IP : [mp4](./07_architecture_reseau/01_cours_reseau/3_MOOC_SNT_Internet_Protocole_TCP_IP.mp4)
- Fiche sur les protocoles IP et TCP : [pdf](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf) ou [odt](./07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.odt)
- TP sur la construction et la configuration d'un réseau type internet : [pdf](./07_architecture_reseau/02_contruction_configuration_reseau/Filius_NSI.pdf)

- TP sur la construction d'un réseau avec serveur DNS : [pdf](./07_architecture_reseau/TP_DNS.pdf)


## Algorithmique - recherche dichotomique

- TP cours : [pdf](./10_algorithmique/Recherche_dichotomique.pdf)


## Algorithmique - algorithmes gloutons

- TP cours : [pdf](./10_algorithmique/Algorithmes_Gloutons.pdf)


## Projets

- Le permis à point : [pdf](../projets/permis_a_point/permis_a_point.pdf) ou [odt](../projets/permis_a_point/permis_a_point.odt)
- Jeu du pierre-feuille-ciseaux : [pdf](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux.pdf) ou [odt](../projets/pierre_feuille_ciseaux/pierre_feuille_ciseaux.odt)
- ASCII Art version première : [md](../projets/ascii_art/Ascii_Art.md)
- Un site web intéractif à plusieurs pages sur l'algèbre de Boole : [zip](./08_architecture_ordinateur/01_algebre_booleenne/site_algebre_Boole.zip)
- Un site web en flexbox : [pdf](./06_ihm_web/02_site_flexbox/site_web_Flexbox.pdf) ou [odt](./06_ihm_web/02_site_flexbox/site_web_Flexbox.odt)
    * Ressources pour le TP flexbox : [zip](./06_ihm_web/02_site_flexbox/images.zip)
    * Ajout de pages au site : [pdf](./06_ihm_web/02_site_flexbox/Ajout_pages_web.pdf) ou [odt](./06_ihm_web/02_site_flexbox/Ajout_pages_web.odt)


## Algorithmique - Recherche dichotomique

- Illustration de la recherche dichotomique : [pdf](./10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.pdf) ou [odp](./10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.odp)
- Travail pratique autour de la recherche dichotomique : [md](./10_algorithmique/TP_algo_03_recherche_dichotomique.md)


## Python - Démarrer

- Diapo cours : [pdf](./02_python_demarrer/Cours_Python_01_presentation.pdf) ou [odp](./02_python_demarrer/Cours_Python_01_presentation.odp)
- TP-cours pour démarrer en Python : [pdf](./02_python_demarrer/Cours_Python_01_Demarrer_en_Python.pdf) ou [odt](./02_python_demarrer/Cours_Python_01_Demarrer_en_Python.odt)
- Frise histoire des langages de programmation : [pdf](./02_python_demarrer/Frise_Histoire_Langages.pdf)


## Interaction client-serveur (protocoles TCP, HTTP, HTTPS)

La mise en pratique nécessite l'installation de [Wireshark](https://www.wireshark.org/). Il est disponible sous Linux, macOS et Windows. Sous ce dernier l'installation du driver [Npcap](https://nmap.org/npcap/) sera nécessaire pour acquérir les données échangées entre deux machines différentes.

L'activité peut très bien se faire en local sur la même machine. Elle sert alors à la fois de serveur et de client. Les applications serveur et client seront donc lancées en parallèle. Il faut pour cela utiliser les adresses de loopback. L'adresse IP client sera 127.0.0.1 et l'adresse IP serveur sera 127.0.0.X avec X entre 2 et 254, par exemple 127.0.0.20.

Enfin, l'activité pourrait être simulée sous [Filius](https://www.lernsoftware-filius.de/Herunterladen) à l'aide du mini-réseau construit dans le cours précédent.

- Notions de cours :
  * Fiche décrivant la mise en place d'un serveur Python : [pdf](./06_ihm_web/03_client_serveur/04a_Fiche_eleve_Serveur_python.pdf) ou [odt](./06_ihm_web/03_client_serveur/04a_Fiche_eleve_Serveur_python.odt)
  * Fiche décrivant la mise en place d'un serveur Python sécurisé : [pdf](./06_ihm_web/03_client_serveur/04e_Fiche_eleve_Serveur_python_securise.pdf) ou [odt](./06_ihm_web/03_client_serveur/04e_Fiche_eleve_Serveur_python_securise.odt)
  * Fiche rappelant quelques notions sur le protocole HTTP : [pdf](./06_ihm_web/03_client_serveur/04c_Activite_SNT_Fiche_eleve_HTTP.pdf) ou [odt](./06_ihm_web/03_client_serveur/04c_Activite_SNT_Fiche_eleve_HTTP.odt)
- Mise en pratique autour d'un site web statique puis dynamique :
  * Sujet : [pdf](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.pdf) ou [odt](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.odt)
  * Fichiers nécessaires [zip](./06_ihm_web/03_client_serveur/TP_IHM_WEB_HTTP_HTTPS.zip)


## Architecture réseau : protocole du bit alterné

- Notions de cours : [pdf](./07_architecture_reseau/03_bit_alterne/bit_alterne.pdf) ou [odp](./07_architecture_reseau/03_bit_alterne/bit_alterne.odp)


