# Opérations sur les nombres en base 2

### Addition

L'addition en base 2 se déroule de la même manière qu'une addition en base 10.

#### Bases de l'opération

* $1+0 = 1_{2}$
* $1+1 = 10_{2}$
  <u>Rappel</u> : $10_{2} = 2_{10}$
  On a bien $1_{10} + 1_{10} = 2_{10} <==> 1_{2} + 1_{2} = 10_{2}$.

La retenue s'obtient alors quand on doit additionner 1 avec 1.

**Exemple posé** :

<img src="addition_binaire.png"/>

### Soustraction

La soustraction fonctionne de la même manière que l'addition.
Le cas de $0-1$ implique l'utilisation d'une retenue au chiffre du nombre que l'on soustrait.
Au chiffre de la colonne suivante du nombre que l'on soustrait, on ajoute 1, comme pour une soustraction en base 10.
De cette manière, on aura $10_{2} - 1_{2} = 1_{2}$. 
On n'oubliera pas de rajouter 1 au chiffre suivant dans le nombre que l'on soustrait.

**Exemple posé** :

<img src ="soustraction_binaire.jpeg"/>

### Multiplication

La multiplication est une opération très simple à réaliser en base 2 comme elle se base que sur 0 ou 1.

<img src="multiplication_binaire.jpeg"/>


### Multiplication ou division par 2

Lorsque l'on multiplie un nombre par 2 en base 2, cela revient à décaler les bits d'un rang sur la gauche. 
En effet, comme 2 est la base dans laquelle on opère, multiplier par la base revient à décaler les chiffres de colonnes.
**Exemple** : En base 10, lorsque l'on multiplie par 10 :  $42_{10} \times 10_{10} = 420_{10}$.
**Exemple** : En base 2, lorsque l'on multiplie par 2 : $11_{2} \times 10_{2} = 110_{2}$
De la même manière, lorsque l'on divise par 2 un nombre en base 2, cela revient à décaler les bits d'un rang sur la droite.
**Exemple** : En base 10, lorsque l'on divise par 10 : $89 \vert 10 = 8.9$.

Cela est dû au fait que lorsque l'on multiplie ou divise par la base, on augmente ou on diminue la puissance de la base pour chacune des positions.
