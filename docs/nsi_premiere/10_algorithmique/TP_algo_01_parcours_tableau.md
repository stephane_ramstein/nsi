# Parcours séquentiel d’un tableau


## I. Calcul d'une moyenne

Pour calculer la moyenne d'une liste de données il suffit de parcourir ces données en les additionnant. Le résultat est divisé par le nombre total de données. L'algorithme est donc le suivant.

```
somme ← 0

BOUCLE pour i allant de 0 à longueur de la liste - 1
  somme ← somme + élément i de la liste

moyenne ← somme / longueur de la liste
```

1\. Coder l'algorithme en Python et le tester sur une liste de 30 nombres aléatoires entre 0 et 100. Cette liste sera générée par compréhension avec `liste_nombres = [random.randint(0, 100) for i in range(30)]` grâce au module `random`.

2\. Montrer que la complexité en temps de cet algorithme est $`\mathrm{T(n) = 2.n + 3}`$, avec $`\mathrm{n}`$ la longueur de la liste.

3\. Montrer que l'algorithme se termine.

4\. Montrer que l'algorithme est correct.

Avec Python on peut évaluer le temps d'exécution d'un algorithme. On peut ainsi en partie vérifier la relation trouvée pour sa compléxité en temps, si on utilise la même machine avec le même environnement, dans exactement les mêmes conditions. Pour cela on utilise la fonction `perf_counter()` du module `time` qui donne un instant en seconde chronométré par l'horloge du microprocesseur depuis le début de l'éxécution du programme. Ainsi le code suivant permet d'estimer le temps d'exécution de l'algorithme. Attention, il est important de de bien noter que si l'environnement et les conditions sont différents entre deux exécutions de l'algorithme, même avec le même ordinateur, les résultats ne seront probablement pas valables.

5\. Ecrire le programme suivant en insérant votre algorithme à la ligne 8.

 ```python
  1|  import time
  2|  
  3|  n = 30
  4|  liste_nombres = [random.randint(0, 100) for i in range(30)]
  6|  
  6|  t_debut = time.perf_counter()
  7|  
  8|  ## INSERER L'ALGORITHME ICI
  9|  
10|  t_fin = time.perf_counter()
11|  
12|  print('''temps d'exécution :''', t_fin - t_debut)
```

6\. Exécuter l'algorithme pour différentes valeurs de n. On prendra `n = 20`, `n = 40`, `n = 60`, ..., jusque `n = 200`. Noter les valeurs obtenues pour le temps d'exécution $`\mathrm{\Delta t}`$.

7\. Tracer le nuage de points $`\mathrm{\Delta t = f(n)}`$. Quel type de courbe obtient-on ? Est-ce attendu ? Expliquer.

8\. Peut-on tenir compte des valeurs obtenues pour les coefficients ? Expliquer.


## II. Recherche d'un extrémum

Pour rechercher un maximum (minimum), qui est le plus grand (petit) élément, d'une liste, il suffit de parcourir la liste en comparant les éléments les uns après les autres et de conserver la valeur, éventuellement la position, de l'élement le plus grand (petit). Dans le cas de la recherche d'un maxiumum, l'algorithme est donné ci-dessous.


```
valeur_max ← élément 0 de la liste

BOUCLE pour i allant de 1 à longueur de la liste - 1
  SI élément i de la liste > valeur_max
    valeur_max ← élément i de la liste
```

On obitient l'algorithme de recherche d'un minimum en inversant le signe du test.

1\. Coder l'algorithme en Python et le tester sur une liste de 30 nombres aléatoires entre 0 et 100.

2\. Montrer que la complexité en temps dans le pire des cas de cet algorithme est $`\mathrm{T(n) = 2.n - 1}`$, avec $`\mathrm{n}`$ la longueur de la liste.

3\. Montrer que l'algorithme se termine.

4\. Exécuter l'algorithme pour différentes valeurs de n. On prendra `n = 20`, `n = 40`, `n = 60`, ..., jusque `n = 200`. Noter les valeurs obtenues pour le temps d'exécution $`\mathrm{\Delta t}`$. On travaillera dans le pire des cas, c'est à dire lorsque la liste de données est triée dans l'ordre croissant.

5\. Tracer le nuage de points $`\mathrm{\Delta t = f(n)}`$. Quel type de courbe obtient-on ? Est-ce attendu ? Expliquer.


## III. Recherche d'une occurence

Pour rechercher une occurence d'un élément d'une liste, il suffit de balayer la liste en vérifiant la correspondance avec l'élément recherché. Dès qu'il y a correspondance la recherche s'arrête. Le résultat est l'indice de l'élément recherché s'il existe.

```
occurence_trouvee ← Faux
i ← 0

BOUCLE tant que occurence_trouvee n'est pas égale à Vrai et i <= longueur de la liste - 1
  SI l'élément i de la liste est égale à l'élément recherché
    occurence_trouvee ← Vrai
  SINON
    i ← i + 1
```

1\. Coder l'algorithme en Python et le tester sur une liste de 30 nombres aléatoires entre 0 et 100. On recherchera l'indice d'éléments existants et non existants.

2\. Montrer que la complexité en temps dans le pire des cas de cet algorithme est $`\mathrm{T(n) = 6.n + 2}`$, avec $`\mathrm{n}`$ la longueur de la liste.

3\. Montrer que l'algorithme se termine.

4\. Exécuter l'algorithme pour différentes valeurs de n. On prendra `n = 20`, `n = 40`, `n = 60`, ..., jusque `n = 200`. Noter les valeurs obtenues pour le temps d'exécution $`\mathrm{\Delta t}`$. On travaillera dans le pire des cas, c'est à dire lorsque l'on recherche une valeur non présente dans la liste.

5\. Tracer le nuage de points $`\mathrm{\Delta t = f(n)}`$. Quel type de courbe obtient-on ? Est-ce attendu ? Expliquer.
