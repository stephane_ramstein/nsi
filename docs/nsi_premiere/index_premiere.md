# Classe de première


## Consignes de rentrée

- **matériel obligatoire** : évalué chaque trimestre
    - un classeur avec des pochettes transparentes : pour ranger l'ensemble du cours dans l'ordre. Pour chaque chapitre il faudra utiliser :
        - une pochette pour le titre
        - deux pochettes pour le cours : une pour le cours et une autre pour la prise de notes
        - deux pochettes par TP ou TD : une pour le sujet et une autre pour la prise de notes
    - une clé USB : pour sauvegarder programmes et documents. Ne pas oublier de sauvegarder son contenu chez soi en cas de perte ou de dysfonctionnement.

- **le cours :** il s'articule entre cours au tableau appuyé d'un diaporama, lecture du livre de NSI à la maison, travaux pratiques avec ou sans ordinateur et travaux dirigés sans ordinateur. Par séance, il faut à minima noter :
    - la date du jour
    - les définitions, explications, remarques, ...
    - les réponses aux questions posées
    - ce qu'on a fait pendant la séance
    - ce qu'il y a à faire pour la prochaine séance

- **remarques sur la prise de notes :**
    - elle ne doit pas forcément être propre ou bien rédigée
    - elle doit être structurée
    - elle doit contenir les explications de l'enseignant, les exemples, les réponses aux questions, les corrections, ...
    - elle doit contenir toutes les informations utiles pour l’apprentissage et les révisions

- **le travail à la maison :** il consiste à rédiger au propre le cours, lire le livre de NSI, faire les exercices, devoirs à la maison, avancer les projets et réviser le tout régulièrement.

- **les évaluations :** elles consistent en des devoirs surveillés écrits et pratiques sanctionnés par une note sur 20. Les devoirs à la maison, les oraux, les projets, la prise de notes, et la **tenue du classeur** sont aussi évalués sous la forme d'un bonus entre 0 et 2 points. La moyenne des bonus du trimestre s'ajoute à la moyenne des devoirs surveillés. Ainsi, un travail personnel et sérieux permet facilement d'augmenter sa moyenne de 1 point même s'il est incomplet ou contient des erreurs. Un travail non rendu est sanctionné par un malus de 0.5 points. Un travail bâclé ou recopié est automatiquement évalué à 0 points.


**A FAIRE dès le premier cours :**

<!-- - Prise en main de la rédaction en markdown : [md](../outils/initiation_markdown.md) -->

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/) **(progression évaluée en cours)**

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/) **(progression évaluée en cours et à la maison, niveaux 1 et 2 validés fin de semaine prochaine + niveau 3 jusqu'au chapitre 7 inclus validés fin de semaine suivante)**


## Langages et programmation - Mise au point des programmes, gestion des bugs

Un langage de programmation est un langage formel. Son utilisation impose une rigueur mathématique pour l'écrire, l'interpréter et obtenir le bon comportement de la machine. Programmer n'est donc pas une tâche facile. Tout au long de l'apprentissage, le novice, tout au long de son travail, l'expert, rencontre erreurs et bugs. L'expérience personnelle et l'application des bonnes pratiques, issues de l'expérience des experts, permettra d'améliorer quotidiennement la qualité des programmes rédigés.

Pendant l'année, les problématiques classiques seront exposées, les situations rencontrées seront expliquées avec propositions de solutions.

- Prise en main de Python et bonnes pratiques : [md](../outils/initiation_python.md)

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/)

- Parcours Castor : [lien](https://concours.castor-informatique.fr/)

- Parcours Algorea : [lien](https://parcours.algorea.org/contents/4703/)

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/)


## Python - Constructions élémentaires - Programmation structurée

Les paradigmes de programmation impérative et structurée sont présentés en Python. La programmation structurée est opposée à la programmation dite "spaghetti" largement diffusée à travers le langage BASIC, qui a permis aux amateurs éclairés des années 70 de s'initier à la programmation et aux petites entreprises d'intégrer l'outil informatique. C'est en développant et commercialisant ce langage pour l'Altaïr 8800, premier micro-ordinateur grand public, que Bill Gates, agé de 19 ans, et Paul Allen, agé de 22 ans, fondent Micro-soft en 1975.

- TP-cours sur le gestion du flux d'instructions : [pdf](./03_python_constructions_elementaires/Cours_Python_02_Gestion_flux_instructions.pdf) ou [odt](./03_python_constructions_elementaires/Cours_Python_02_Gestion_flux_instructions.odt)
- Fiche d'exercices sur la structure conditionnelle : [pdf](./03_python_constructions_elementaires/exos_if.pdf) ou [odt](./03_python_constructions_elementaires/exos_if.odt)
- Fiche d'exercices sur les structures itératives : [pdf](./03_python_constructions_elementaires/exos_for_while.pdf) ou [odt](./03_python_constructions_elementaires/exos_for_while.odt)
- Compléments sur l'affectation et le typage des données : [pdf](./02_python_demarrer/Complements_affectation_typage.pdf) ou [odt](./02_python_demarrer/Complements_affectation_typage.odt)
- Compléments sur les fonctions `print` et `input` : [pdf](./02_python_demarrer/Complements_print_input.pdf) ou [odt](./02_python_demarrer/Complements_print_input.odt)


## Architecture de l'ordinateur : l'ordinateur de Von Neumann

L'idée d'ordinateur comme nous la connaissons ne s'est pas faite en un jour. Elle s'est construite sur une longue évolution des idées mathématiques et des techniques, d'abord mécaniques, puis électroniques, qui remonte à plus de 40000 ans avant notre ère, époque de l'Homme de Cro-Magnon. C'est en effet notre besoin de compter, mémoriser et calculer qui nous a conduit à construire des machines nous permettant de le faire de manière plus efficace. Cette évolution a été marquée par trois accélérations remarquables. Au XIIIème siècle, l'avènement de la mécanique de précision a introduit l'automatisation des machines. Au XIXème siècle, l'industrialisation de l'électricité rend les machines électriques et au milieu du XXème siècle, Turing, Von Neumann et leurs collaborateurs définissent et construisent l'ordinateur moderne. La miniaturisation du composant principal des machines, le transistor, n'a eu de cesse de décupler les performances des ordinateurs jusqu'à ce jour.

Après la présentation de l'architecture de Von Neumann des processeurs, leur programmation en langage machine et en assembleur est abordée.

- Cours : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/Cours_NSI_Architecture.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/Cours_NSI_Architecture.odp)
- Activités pour comprendre le fonctionnement d'un processeur et l'assembleur : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/TP_assembleur.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/TP_assembleur.odt)
- Fiches processeur vierges : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/schema_neumann_vierge.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/schema_neumann_vierge.odp)
- Compléments sur les processeurs et les mémoires : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/processeurs_et_memoires.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/processeurs_et_memoires.odp)


## Représentation des données : les entiers naturels

- Diapo cours histoire de la numération binaire : [pdf](./05_representation_donnees/01_présentation/presentation_numeration.pdf) ou [odp](./05_representation_donnees/01_présentation/presentation_numeration.odp)
- Archives de l'Académie Royale des Sciences avec l'article de Leibnitz : [pdf](./05_representation_donnees/01_présentation/Archives_academie_des_sciences.pdf)
<!-- - TP-Cours sur les nombres entiers : [md](./05_representation_donnees/02_numeration_entiers_naturels/Braun_Clement_Cours.md) -->

<!-- - TD sur les nombres entiers : [md](./05_representation_donnees/02_numeration_entiers_naturels/Braun_Clement_Operations_binaires.md) -->


- TP_cours sur les entiers naturels : [pdf](./05_representation_donnees/02_numeration_entiers_naturels/les_entiers_naturels.pdf) ou [odt](./05_representation_donnees/02_numeration_entiers_naturels/les_entiers_naturels.odt)
    - Tableau à compléter des nombes de 0 à 255 en base 10, 2 et 16 : [pdf](./05_representation_donnees/02_numeration_entiers_naturels/fiche_nombre_bases.pdf) ou [ods](./05_representation_donnees/02_numeration_entiers_naturels/fiche_nombre_bases.ods)
- Fiche exercices sur les entiers naturels : [pdf](./05_representation_donnees/02_numeration_entiers_naturels/exos_entiers_non_signes.pdf) ou [odt](./05_representation_donnees/02_numeration_entiers_naturels/exos_entiers_non_signes.odt)



## Représentation des données : les entiers relatifs

- TP-cours sur les entiers relatifs : [pdf](./05_representation_donnees/03_entiers_relatifs/Cours_Representation_nombres_relatifs.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/Cours_Representation_nombres_relatifs.odt)
- Videos pour comprendre le codage des entiers naturels :
  * fonctionnement d'un odomètre : [mp4](./05_representation_donnees/03_entiers_relatifs/mechanical-odometer-1.mp4)
  * odomètre à rebours : [mp4](./05_representation_donnees/03_entiers_relatifs/registre_calculatrice_cpt_a_rebours.mp4)
- Fiche exercices sur les entiers relatifs : [pdf](./05_representation_donnees/03_entiers_relatifs/exos_entiers_signes.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/exos_entiers_signes.odt)
<!-- - TP Python sur la représentation des entiers : [pdf](./05_representation_donnees/03_entiers_relatifs/TP_Représentation_des_nombres.pdf) ou [odt](./05_representation_donnees/03_entiers_relatifs/TP_Représentation_des_nombres.odt) -->


## Python - Parcours de tableaux et dictionnaires

- Travail pratique sur les parcours de base : [pdf](./04_python_sequences/Parcours_Vanoverberghe.pdf) ou [odt](./04_python_sequences/Parcours_Vanoverberghe.odt)

- Travail pratique sur les dictionnaires et les tableaux 2D : [pdf](./04_python_sequences/dico_matrices_Vanoverberghe.pdf) ou [odt](./04_python_sequences/dico_matrices_Vanoverberghe.odt)


## Fonctionnement électrique de l'ordinateur - Circuits combinatoires

<!-- - Cours sur les portes, fonctions et circuits logiques : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/Les_circuits_electroniques.pdf)
- Fiche d'excercices : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/Exercices_Circuits_logiques.pdf) -->

- Notions de cours :
    * Fiche décrivant les principales fonctions logiques utilisées en électronique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Electronique_et_logique.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Electronique_et_logique.odt)

    * Fiche décrivant les signaux électriques transmis en informatique [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Signaux_numériques.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_Signaux_numériques.odt)

    * Article sur la problématique du Dark Silicon [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/Dark_Silicon.pdf)


- Fiche TD sur les circuits logiques : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_TP_Representation_electrique_information_papier.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/SR_05_TP_Representation_electrique_information_papier.odt)
    * Circuit de l'additionneur 4 bits avec registres Flip-flop en entrée et en sortie : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit.pdf)
        - circuit Logisim : [circ](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_4_bits.circ)
    * Circuit de l'additionneur 4 bits avec accumulateur D-Flip-Flop : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit_add_with_acc.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/schema_circuit_add_with_acc.pdf)
        - circuit Logisim : [circ](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_4_bits_accumulateur_detaille.circ)
    * Aide pour étudier le fonctionnement de l'additionneur série : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_serie.pdf) ou [odp](./08_architecture_ordinateur/02_circuits_combinatoires/additionneur_serie.odp)

- TP Python sur les circuits combinatoires : [pdf](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques_python.pdf) ou [odt](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques_python.odt)
    * Ressources : [py](./08_architecture_ordinateur/02_circuits_combinatoires/fonctions_logiques.py)

- Construire un ordinateur de A à Z : [lien](https://nandgame.com/)

- Schéma électronique de l'UAL 4 bits 74181 : [pdf](./08_architecture_ordinateur/04_von_neumann_et_assembleur/ual_4bits_74181.pdf) ou [odp](./08_architecture_ordinateur/04_von_neumann_et_assembleur/ual_4bits_74181.odp)
    - circuit Logisim : [circ](./08_architecture_ordinateur/02_circuits_combinatoires/UAL_74181.circ)


## Représentation des données : les nombres réels

- TD-cours : [pdf](./05_representation_donnees/04_reels/flottants_cours.pdf) ou [odt](./05_representation_donnees/04_reels/flottants_cours.odt)
- Illustrations de cours : [pdf](./05_representation_donnees/04_reels/flottants_cours_illustrations.pdf) ou [odp](./05_representation_donnees/04_reels/flottants_cours_illustrations.odp)
- Fiche exercices : [pdf](./05_representation_donnees/04_reels/flottants_exercices.pdf) ou [odt](./05_representation_donnees/04_reels/flottants_exercices.odt)


## Python : les dictionnaires

Les dictionnaires sont des structures de données mutables qui permettent d'identifier chacun de leurs éléments grâce à une clé, qui peut être une chaîne de caractères, un nombre, ...

- Diaporama de cours : [pdf](./11_python_dictionnaires/cours_dicos.pdf) ou [odp](./11_python_dictionnaires/cours_dicos.odp)
- TP python : [pdf](./11_python_dictionnaires/tp_dictionnaires.pdf) ou [odt](./11_python_dictionnaires/tp_dictionnaires.odt)
- Exercices python pour s'entraîner : [pdf](./11_python_dictionnaires/Cours_Python_04e_Les_dictionnaires.pdf) ou [odt](./11_python_dictionnaires/Cours_Python_04e_Les_dictionnaires.odt)


## Systèmes d’exploitation

- Diapo cours : [pdf](./01_systemes_exploitation/cours_OS.pdf) ou [odp](./01_systemes_exploitation/cours_OS.odp)
- Extrait de licence Windows : [html](./01_systemes_exploitation/UseTerms_Retail_Windows_10_French.htm)
- Frise histoire d'UNIX : [png](./01_systemes_exploitation/Frise_Histoire_Unix.png)
- Frise histoire de Windows : [pdf](./01_systemes_exploitation/Frise_Histoire_Windows.pdf)
<!-- - TP version Linux : [pdf](./01_systemes_exploitation/TP_OS_linux.pdf) ou [odt](./01_systemes_exploitation/TP_OS_linux.odt) -->
- Jeu pour apprendre les commandes Linux : [Terminus en français](https://luffah.xyz/bidules/Terminus/) et [Terminus en VO](https://www.mprat.org/Terminus/)
<!-- - TP sur le gestion des droits d'accès aux fichiers et dossiers sous Linux : [pdf](./01_systemes_exploitation/TP_droits.pdf) ou [odt](./01_systemes_exploitation/TP_droits.odt)
- Fiche sur le gestion des droits d'accès aux fichiers et dossiers sous Linux : [pdf](./01_systemes_exploitation/diapo_droits.pdf) ou [odp](./01_systemes_exploitation/diapo_droits.odp) -->

<!-- - TP version Windows : [pdf](./01_systemes_exploitation/TP_OS.pdf) ou [odt](./01_systemes_exploitation/TP_OS.odt) -->


## Projets

- ASCII Art version première : [md](../projets/ascii_art/Ascii_Art.md)
- La machine à inventer des mots :
    - Vidéo introductive : [Science étonnante](https://sciencetonnante.wordpress.com/2015/11/06/la-machine-a-inventer-des-mots-version-ikea/)
    - Guide Python : [pdf](../projets/machine_mots/machine_inventer_mots.pdf) ou [odp](../projets/machine_mots/machine_inventer_mots.odp)
        * Ressources : [zip](../projets/machine_mots/ressources_mots.zip)


## Corrections

[Dépôt des corrections](https://gitlab.com/stephane_ramstein/corrections/-/tree/main/1NSI)
