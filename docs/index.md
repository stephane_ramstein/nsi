# Spécialité Numérique et Sciences Informatiques

*Stéphane Ramstein : <stephane.ramstein@ac-lille.fr>*

*Enseignant d'Informatique et de Physique-Chimie au lycée Raymond Queneau de Villeneuve d'Ascq*


[Participer au concours Prologin](https://prologin.org/)
![Alt text](./assets/prologin.png)

[Participer aux stages Girls Can Code et revenir avec du matériel](https://girlscancode.fr/)
![Alt text](./assets/girlscancode.png)


--------------------

## Présentation

Présentation de la spécialité NSI : [pdf](./presentation_programme_NSI.pdf) ou [odp](./presentation_programme_NSI.odp)

Vidéos sur les métiers de l'informatique et du numérique :
- ONISEP "Les métiers du numériques" : [lien](https://oniseptv.onisep.fr/video/les-metiers-du-numerique)
- ONISEP "Les métiers de l'informatique" [lien](https://oniseptv.onisep.fr/video/les-metiers-de-linformatique)
- ONISEP "Développeur informatique" : [lien](https://oniseptv.onisep.fr/video/developpeur-informatique)
- ZOOM "Jobs en informatique et multimédia" : [lien](https://zoom-vd.ch/fr/metiers/zoom-sur-les-metiers/detail-metiers/jobs-informatique-multimedia)

Supports de cours, TD et TP pour la spécialité NSI.

* [Classe de première](./nsi_premiere/index_premiere.md)
* [Classe de terminale](./nsi_terminale/index_terminale.md)

Lien vers le dépôt GitLab des ressources :

* [Dépôt GitLab](https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs)

Lien vers la page web image du dépôt :

* [Page web du dépôt](https://stephane_ramstein.gitlab.io/nsi)


## Les décodeuses du Numérique

![](assets/index-0cc56078.png)

12 portraits de chercheuses, enseignantes-chercheuses et ingénieures dans les sciences du numérique, croquées par le crayon de Léa Castor. L'Institut des sciences de l'information et de leurs interactions (INS2I) du CNRS met en avant la diversité des recherches en sciences du numérique à travers l'implication des femmes dans ce domaine.

[La page web](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique)

[La BD en consultation libre](https://www.calameo.com/read/006841715804996467dcf?authid=cG5djzzVzuiW)

[La BD en pdf](./les_decodeuses_du_numerique_web.pdf)


## Programmes de la NSI

* Programme de première NSI : [pdf](./nsi_premiere/Programme_1NSI.pdf)
* Programme de terminale NSI : [pdf](./nsi_terminale/Programme_TNSI.pdf)
* Points du programme de terminale qui ne figurent pas aux épreuves écrite et pratique: [md](./nsi_terminale/adaptation_epreuves_ecrite_pratique.md)


## Outils pour la NSI

* Logiciels : [md](./outils/logiciels.md)
* Prise en main du langage Python : [md](./outils/initiation_python.md)
* Modules python : [md](./outils/modules_python.md)
* Prise en main du microcontrôleur micro:bit : [md](./outils/initiation_microbit.md)
* Le raspberry PI : [md](./outils/raspberry_pi.md)
* Capteurs et actionneurs pour micro:bit et Raspberry Pi: [md](./outils/capteurs_actionneurs.md)
* Utilisation d'un drone Tello avec Python : [md](./outils/drone_tello.md)
* Prise en main de la rédaction en markdown : [md](./outils/initiation_markdown.md)
* Les quaternions : [md](./outils/quaternions.md)
* Un serveur python : [md](./outils/serveur_python.md)


## Le baccalauréat NSI

* Résumé des épreuves 2024 en première et en terminale : [pdf](./Bac_NSI.pdf)
* Baccalauréat général 2024 : [lien](https://www.education.gouv.fr/reussir-au-lycee/le-baccalaureat-general-10457)
    * Calendrier des épreuves : [pdf](./Calendrier_epreuves_bac_2024.pdf)
    * Coefficients : [jpg](./Coefficients_epreuves_bac_2024.jpg)
    * Calculer sa note : [pdf](./calcul_note_bac_2024.pdf)
* Sujets des épreuves écrites de première du baccalauréat 2020 et 2021 :
    - Banque nationale des sujets de première : [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_premiere/Sujets_Bac)
* Sujets des épreuves écrites et pratiques de terminale :
    - Banque nationale des épreuves pratiques de terminale : [lien](https://cyclades.education.gouv.fr/delos/public/listPublicECE) et [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_terminale/Sujets_Bac/epreuves_pratiques)
    - Propositions de corrections (non officielles) et commentaires sujets Bac 2024 :
        - [Pixees](https://pixees.fr/informatiquelycee/term/index.html#ep_prat)
        - [Math93](https://www.math93.com/annales-du-bac/bac-specialite-nsi/annales-nsi-2024/nsi-pratique-2024.html)
        - [ECEBac](https://ecebac.fr/listaca.php?mat=94)
    - Propositions de corrections et commentaires sujets Bac 2023 : [lien](https://fabricenativel.github.io/Terminale/Annales/2023/EP/)
    Sujets écrits complets et corrigés : [lien](https://pixees.fr/informatiquelycee/term/suj_bac/)
    <!-- - Sujets écrits complets et fractionnés par thèmes : [dépôt](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/nsi_terminale/Sujets_Bac/epreuves_ecrites) -->
* Grand Oral :
    - document de présentation : [pdf](./Grand_oral_Igesr_Document_integral_0.pdf)
    - foire aux questions : [pdf](./Grand-oral_FAQ_enseignants.pdf)
    - grille d'évaluation : [pdf](./Grand-oral_grille_d_evaluation.pdf)


## Les concours et stages niveau Lycée

* Concours Castor informatique : [lien](https://castor-informatique.fr/)
![](assets/20230114160722.png)

* Concours Algorea : [lien](https://www.algorea.org/)
![](assets/20230114160518.png)

* Olympiades de NSI premières : [lien](https://pedagogie.ac-lille.fr/numerique-et-sciences-informatiques/olympiades-academiques-de-nsi-2024/)
![](assets/olympiades_nsi_premieres.jpg)

* Trophées de nsi : [lien](https://trophees-nsi.fr/)
![](assets/20230114155858.png)

* Olympiades d'informatique : [lien](http://www.france-ioi.org/ioi/index.php)
![](assets/20230114160417.png)  

* Prologin : [lien](https://prologin.org/)
![](assets/20230114155837.png)

* Girls Can Code : [lien](https://prologin.org/)
![Alt text](./assets/girlscancode_mini.png)


## L'orientation en informatique à lille

* Présentation réalisée par nos stagiaires de M2 : [pdf](./orientation_lille.pdf)


## La classe préparatoire MP2I

* Au lycée Jean-Baptiste Colbert : [pdf](./Diaporama2023-Colbert-Queneau.pdf)
* Programme d'Informatique de première année : [pdf](./programme-mp2i_informatique.pdf)
* Programme de Mathématiques de première année : [pdf](./programme-mpsi-mp2i_mathematiques.pdf)
* Programme de Physique-Chimie de première année : [pdf](./programme-mp2i_phys_chim.pdf)
* Programme de Sciences Industrielles de l'Ingérnieur de première année : [pdf](./programme-mp2i_si.pdf)
* Programme d'Informatique de deuxième année : [pdf](./mpi_info.pdf)
* Programme de Mathématiques de deuxième année : [pdf](./mp_mpi_maths.pdf)
* Programme de Physique-Chimie de deuxième année : [pdf](./mpi_phys_chim.pdf)


## Ressources d'autres collègues

D'autres collègues ont mis en ligne des cours détaillés et fournis en activités :

* [Dépôt Framagit de Ghesquière Cécile en 1NSI](https://framagit.org/CecGhesq/1_nsi_21)
* [Dépôt Framagit de Ghesquière Cécile en TNSI](https://framagit.org/CecGhesq/t_nsi_21)
* [Dépôt Framagit de Mieszczak Christophe](https://framagit.org/tofmzk/informatique_git/-/tree/master/)
* [Infoforall](https://www.infoforall.fr/) : l'informatique pour tous
* [Pixees](https://pixees.fr/informatiquelycee/) : informatique au lycée
* [Lycée Blaise Pascal de Clermont Ferrand](https://info.blaisepascal.fr/) : l'informatique, c'est fantastique !
* [Lycée Angellier de Dunkerque](http://vfsilesieux.free.fr/)
* [Lycée Jean Moulin de Dradignan](https://isn-icn-ljm.pagesperso-orange.fr/)
* [Page de Laurent Pointal, Ingénieur d'Études CNRS au LIMSI d'Orsay](https://perso.limsi.fr/pointal/python:accueil)


## Bibliographie

* [Wikipedia](https://fr.wikipedia.org)
* [Openclassrooms](https://openclassrooms.com/fr/)
* [Université de Lille - Formation à l'enseignement de l'informatique](http://fe.fil.univ-lille1.fr)
* [SUPINFO International University](https://www.supinfo.com/)
* Ellipse : Spécialité Numérique et sciences informatiques : 30 leçons avec exercices corrigés - Première - Nouveaux programmes
* Ellipse : Spécialité Numérique et sciences informatiques : 24 leçons avec exercices corrigés - Terminale - Nouveaux programmes
* Nathan : Interros des Lycées Numérique Sciences Informatiques - Terminale
* Hachette : Numérique et Sciences Informatiques 1re Spécialité - Livre élève - Ed. 2021

--------------------
