# Ressources NSI - Outils


## Modules python

Modules Python d'intérêt et activités pour les prendre en main.

* L'interface graphique **Tkinter** : [pdf](./interfaces_graphiques/interfaces_graphiques_introduction.pdf) ou [odt](./interfaces_graphiques/interfaces_graphiques_introduction.odt)
* Animation avec une interface graphique **Tkinter** : [pdf](./interfaces_graphiques/tkinter_animation.pdf) ou [odt](./interfaces_graphiques/tkinter_animation.odt)
* Utilisation basique de la **Python Imaging Library** : [pdf](./PIL_pillow/PIL_pillow.pdf) ou [odt](./PIL_pillow/PIL_pillow.odt)
* Lire et enregistrer des sons avec **sounddevice** : [pdf](./audio_sous_python/50_python_3_module_sounddevice.pdf) ou [odt](./audio_sous_python/50_python_3_module_sounddevice.odt)
    - Sous Raspberry PI il faut d'abord installer `portaudio` :
    `sudo apt-get install portaudio19-dev`
    - Pour lire des fichiers audio nécessite aussi le module python `soundfile`
    - Fichier sonore : [wav](./audio_sous_python/foret.wav) ou [mp3](./audio_sous_python/foret.mp3)