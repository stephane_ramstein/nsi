# Ressources NSI - Outils


## Utilisation d'un drone Tello avec Python

Les drones Tello peuvent être commandés à partir de Python à travers une liaison WIFI.

* Fiche d'utilisation du drone avec Python : [pdf](./drone_tello/fiche_utilisation_tello.pdf) ou [odt](./drone_tello/fiche_utilisation_tello.odt)
* Documentation du Tello SDK 1.3 : [pdf](./drone_tello/Tello_SDK_Documentation_EN_1.3.pdf)
* Documentation du Tello EDU SDK 2.0 : [pdf](./drone_tello/Tello_SDK_2.0_User_Guide.pdf)
* Fichier Python exemple officiel : [py](./drone_tello/Tello3.py)
* Exemples simplifiés :
    - Commandes avec utilisation directe du SDK : [py](./drone_tello/exemple_socket_controle.py)
    - Vidéo avec utilisation directe du SDK : [py du programme de commande](./drone_tello/exemple_socket_video_commande.py) et [py du programme d'affichage du flux vidéo](./drone_tello/exemple_socket_video_visualisation.py)
    - Vidéo avec la bibliothèque DJITELLOPY : [py](./drone_tello/exemple_djitellopy_video.py)
