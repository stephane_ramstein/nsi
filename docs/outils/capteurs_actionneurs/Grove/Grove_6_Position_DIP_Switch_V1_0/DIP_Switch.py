from microbit import i2c

ADDR = 0x03

_CMD_GET_DEV_ID     = 0x00
_CMD_GET_DEV_EVENT  = 0x01
_CMD_EVENT_DET_MODE = 0x02
_CMD_BLOCK_DET_MODE = 0x03
_CMD_TEST_GET_VER   = 0xE2
VID_MULTI_SWITCH         = 0x2886

class DIP_Switch:
    def __init__(self):
        self.ADDR = ADDR
        self.set_mode(1)

    def uid(self):
        v = _read_bytes(self.ADDR, _CMD_GET_DEV_ID, 4)
        return (v[3] << 8) + v[2]

    def firmware_version(self):
        v = _read_bytes(self.ADDR, _CMD_TEST_GET_VER, 10)
        version = v[6] - ord('0')
        version = version * 10 + (v[8] - ord('0'))
        return version

    def set_mode(self, enable):
        if enable:
            command = _CMD_EVENT_DET_MODE
        else:
            command = _CMD_BLOCK_DET_MODE
        _write_bytes(self.ADDR, command, None)

    def read(self):
        v = _read_bytes(self.ADDR, _CMD_GET_DEV_EVENT, 10)
        return (v[4], v[5], v[6], v[7], v[8], v[9])

def _read_bytes(address, command, length):
    i2c.write(address, bytearray([command]))
    return i2c.read(address, length)

def _write_bytes(address, command, data=None):
    if data == None:
        i2c.write(address, bytearray([command]))
    else:
        i2c.write(address, bytearray([command, data]))
