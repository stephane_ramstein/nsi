import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(5, GPIO.IN)

while True:
    value = GPIO.input(5)
    print(value)
    time.sleep(1)

GPIO.cleanup()