from microbit import *
from i2c_lcd import lcd

# Instanciation de l'afficheur
afficheur = lcd(0x3E)

while True:
    # Positionne le curseur en colonne 1, ligne 1
    colonne = 1
    ligne = 1
    afficheur.setCursor(colonne - 1, ligne -1)

    # Affiche "Hello World"
    afficheur.write('Hello World')

    # Positionne le curseur en colonne 1, ligne 2
    colonne = 1
    ligne = 2
    afficheur.setCursor(colonne - 1, ligne -1)

    # Affiche "Bonjour Monde"
    afficheur.write('Bonjour Monde')

    # Attends deux secondes
    sleep(2000)

    # Efface l'afficheur
    afficheur.clear()

    # Attends deux secondes
    sleep(2000)