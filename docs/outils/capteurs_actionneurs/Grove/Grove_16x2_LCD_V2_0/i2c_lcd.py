'''
date : 2022/05/12
Adapted from micropython to microbit by Stephane Ramstein
stephane.ramstein@ac-lille.fr
'''

from microbit import *
import time

_I2CADDR = 0x3E

# commands
_LCD_CLEARDISPLAY = 0x01
_LCD_RETURNHOME = 0x02
_LCD_ENTRYMODESET = 0x04
_LCD_DISPLAYCONTROL = 0x08
_LCD_CURSORSHIFT = 0x10
_LCD_FUNCTIONSET = 0x20
_LCD_SETCGRAMADDR = 0x40
_LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
_LCD_ENTRYRIGHT = 0x00
_LCD_ENTRYLEFT = 0x02
_LCD_ENTRYSHIFTINCREMENT = 0x01
_LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
_LCD_DISPLAYON = 0x04
_LCD_DISPLAYOFF = 0x00
_LCD_CURSORON = 0x02
_LCD_CURSOROFF = 0x00
_LCD_BLINKON = 0x01
_LCD_BLINKOFF = 0x00

# flags for display/cursor shift
_LCD_DISPLAYMOVE = 0x08
_LCD_CURSORMOVE = 0x00
_LCD_MOVERIGHT = 0x04
_LCD_MOVELEFT = 0x00

# flags for function set
_LCD_8BITMODE = 0x10
_LCD_4BITMODE = 0x00
_LCD_2LINE = 0x08
_LCD_1LINE = 0x00
_LCD_5x10DOTS = 0x04
_LCD_5x8DOTS = 0x00

class lcd:

    def __init__(self, address = _I2CADDR, oneline = False, charsize = _LCD_5x8DOTS):
        i2c.init()
        self.i2c = i2c
        # print(self.i2c.scan())
        self.address = address
        self.disp_func = _LCD_DISPLAYON
        if not oneline:
            self.disp_func |= _LCD_2LINE
        elif charsize != 0:
            # for 1-line displays you can choose another dotsize
            self.disp_func |= _LCD_5x10DOTS

        # wait for display init after power-on
        time.sleep_ms(50) # 50ms

        # send function set
        self.cmd(_LCD_FUNCTIONSET | self.disp_func)
        time.sleep_us(5000) #time.sleep(0.0045) # 4.5ms
        self.cmd(_LCD_FUNCTIONSET | self.disp_func)
        time.sleep_us(200) #time.sleep(0.000150) # 150µs = 0.15ms
        self.cmd(_LCD_FUNCTIONSET | self.disp_func)
        time.sleep_us(5000) #time.sleep(0.0045) # 4.5ms

        # turn on the display
        self.disp_ctrl = _LCD_DISPLAYON | _LCD_CURSOROFF | _LCD_BLINKOFF
        self.display(True)

        # clear it
        self.clear()

        # set default text direction (left-to-right)
        self.disp_mode = _LCD_ENTRYLEFT | _LCD_ENTRYSHIFTDECREMENT
        self.cmd(_LCD_ENTRYMODESET | self.disp_mode)

    def cmd(self, command):
        assert command >= 0 and command < 256
        command = bytes([0x80, command])
        self.i2c.write(self.address, command)

    def write_char(self, c):
        assert c >= 0 and c < 256
        c = bytearray([0x40, c])
        self.i2c.write(self.address, c)

    def write(self, text):
        for char in text:
            self.write_char(ord(char))

    def cursor(self, state):
        if state:
            self.disp_ctrl |= _LCD_CURSORON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)
        else:
            self.disp_ctrl &= ~_LCD_CURSORON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)

    def setCursor(self, col, row):
        col = (col | 0x80) if row == 0 else (col | 0xc0)
        self.cmd(col)

    def autoscroll(self, state):
        if state:
            self.disp_ctrl |= _LCD_ENTRYSHIFTINCREMENT
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)
        else:
            self.disp_ctrl &= ~_LCD_ENTRYSHIFTINCREMENT
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)

    def blink(self, state):
        if state:
            self.disp_ctrl |= _LCD_BLINKON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)
        else:
            self.disp_ctrl &= ~_LCD_BLINKON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)

    def display(self, state):
        if state:
            self.disp_ctrl |= _LCD_DISPLAYON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)
        else:
            self.disp_ctrl &= ~_LCD_DISPLAYON
            self.cmd(_LCD_DISPLAYCONTROL | self.disp_ctrl)

    def clear(self):
        self.cmd(_LCD_CLEARDISPLAY)
        time.sleep_ms(2) # 2ms

    def home(self):
        self.cmd(_LCD_RETURNHOME)
        time.sleep_ms(2) # 2m
