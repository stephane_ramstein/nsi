from microbit import *
from tm1637 import TM1637

tm = TM1637(clk=pin0, dio=pin14)

tm.show('1234', False)