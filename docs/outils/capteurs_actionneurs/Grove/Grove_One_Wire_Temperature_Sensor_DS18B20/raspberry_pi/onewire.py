## Author : Stephane Ramstein
## Date : 2023/03/19
## from https://docs.pycom.io/tutorials/hardware/owd/


## Importation des modules

import RPi.GPIO as GPIO
import time


## Déclaration des constantes

PULL = GPIO.PUD_UP
CMD_SEARCHROM = 0xF0
CMD_READROM = 0x33
CMD_MATCHROM = 0x55
CMD_SKIPROM = 0xCC


## Déclaration des classes

class OneWire:
    def __init__(self, pin):
        self.pin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

    def reset(self):
        """
        Perform the onewire reset function.
        Returns True if a device asserted a presence pulse, False otherwise.
        """
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, GPIO.LOW)
        sleep_us(480)
        GPIO.output(self.pin, GPIO.HIGH)
        sleep_us(60)
        
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=PULL)
        status = not GPIO.input(self.pin)
        sleep_us(420)
        
        return status

    def read_bit(self):
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, GPIO.HIGH) # half of the devices don't match CRC without this line

        GPIO.output(self.pin, GPIO.LOW)
        sleep_us(1)
        GPIO.output(self.pin, GPIO.HIGH)
        sleep_us(1)
        
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=PULL)
        value = GPIO.input(self.pin)
        sleep_us(40)

        return value

    def read_byte(self):
        value = 0
        for i in range(8):
            value |= self.read_bit() << i

        return value

    def read_bytes(self, count):
        buf = bytearray(count)
        for i in range(count):
            buf[i] = self.read_byte()

        return buf

    def write_bit(self, value):
        GPIO.setup(self.pin, GPIO.OUT)
        GPIO.output(self.pin, GPIO.LOW)
        sleep_us(1)
        GPIO.output(self.pin, value)
        sleep_us(60)
        GPIO.output(self.pin, GPIO.HIGH)
        sleep_us(1)

    def write_byte(self, value):
        for i in range(8):
            self.write_bit(value & 1)
            value >>= 1

    def write_bytes(self, buf):
        for b in buf:
            self.write_byte(b)

    def select_rom(self, rom):
        """
        Select a specific device to talk to. Pass in rom as a bytearray (8 bytes).
        """
        self.reset()
        self.write_byte(CMD_MATCHROM)
        self.write_bytes(rom)

    def crc8(self, data):
        """
        Compute CRC
        """
        crc = 0
        for i in range(len(data)):
            byte = data[i]
            for b in range(8):
                fb_bit = (crc ^ byte) & 0x01
                if fb_bit == 0x01:
                    crc = crc ^ 0x18
                crc = (crc >> 1) & 0x7f
                if fb_bit == 0x01:
                    crc = crc | 0x80
                byte = byte >> 1

        return crc

    def scan(self):
        """
        Return a list of ROMs for all attached devices.
        Each ROM is returned as a bytes object of 8 bytes.
        """
        devices = []
        diff = 65
        rom = False
        for i in range(0xFF):
            rom, diff = self._search_rom(rom, diff)
            if rom:
                devices += [rom]
            if diff == 0:
                break

        return devices

    def _search_rom(self, l_rom, diff):
        if not self.reset():
            return None, 0
        
        self.write_byte(CMD_SEARCHROM)
        
        if not l_rom:
            l_rom = bytearray(8)
        
        rom = bytearray(8)
        
        next_diff = 0
        i = 64
        for byte in range(8):
            r_b = 0
            for bit in range(8):
                b = self.read_bit()
                if self.read_bit():
                    if b: # there are no devices or there is an error on the bus
                        return None, 0
                else:
                    if not b: # collision, two devices with different bit meaning
                        if diff > i or ((l_rom[byte] & (1 << bit)) and diff != i):
                            b = 1
                            next_diff = i
                self.write_bit(b)
                if b:
                    r_b |= 1 << bit
                i -= 1
            rom[byte] = r_b

        return rom, next_diff


## Déclaration des fonctions
    
def sleep_us(t):
    t0 = time.perf_counter()
    t1 = t0
    delay = t * 0.000001
    while t1 - t0 < delay:
        t1 = time.perf_counter()


## Programme principal

if __name__ == '__main__':
    one_wire_bus = OneWire(5)
    print(one_wire_bus.scan())