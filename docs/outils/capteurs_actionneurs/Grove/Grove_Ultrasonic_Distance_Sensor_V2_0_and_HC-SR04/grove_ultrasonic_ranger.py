## Written for micropython/microbit
##
## Author : Stephane Ramstein
##
## Should work with other ultrasonic sensors
##
## To use it:
## import grove_ultrasonic_ranger
##
## grove_ultrasonic_ranger.duree_aller_retour() # returns the time of flight of the ultrasonic pulse
## grove_ultrasonic_ranger.distance(vitesse_son) # returns the distance of an object. vitesse_son is the sound speed, around 340 m.s-1
## grove_ultrasonic_ranger.vitesse_son(distance) # returns the sound speed. distance is the distance of an object

import utime
import machine

def duree_aller_retour(pin):
    # Envoi d'une impulsion de 20 us sur la pin0 pour demander
    # au module de lancer une salve ultrasonore
    pin.write_digital(0)
    utime.sleep_us(2)
    pin.write_digital(1)
    utime.sleep_us(20)
    pin.write_digital(0)

    # Configuration de la pin0 en entree numerique
    pin.read_digital()

    # Mesure de la duree de l'aller retour de la salve ultrasonore en us
    duree_aller_retour = machine.time_pulse_us(pin, 1, 1000000)

    return duree_aller_retour

def distance(pin, vitesse_son):
    # Calcul de la distance de l'obstacle (aller) en m
    distance = (duree_aller_retour(pin) / 1000000) * vitesse_son / 2

    return distance

def vitesse_son(pin, distance):
    # Calcul de la distance de l'obstacle (aller) en m.s-1
    vitesse = 2 * distance / (duree_aller_retour(pin) / 1000000)

    return vitesse