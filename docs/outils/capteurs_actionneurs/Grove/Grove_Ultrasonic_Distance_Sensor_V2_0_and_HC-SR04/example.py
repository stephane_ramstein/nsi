from microbit import *
import grove_ultrasonic_ranger

duree = grove_ultrasonic_ranger.duree_aller_retour()
distance = grove_ultrasonic_ranger.distance(340)

print('Le temps du trajet vaut :', duree, 'us')
print('La distance vaut :', distance, 'm')
