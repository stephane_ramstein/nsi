from microbit import i2c, sleep

ADDR = 0x53

class Si1151:
    def __init__(self):
        self.ADDR = ADDR
        self.reset()
        self.defaultInit()

    def uid(self):
        data = _read_bytes(self.ADDR, 0x00, 1)
        return data[0]

    def reset(self):
        _write_bytes(self.ADDR, 0x08, 0)
        _write_bytes(self.ADDR, 0x09, 0)
        _write_bytes(self.ADDR, 0x04, 0)
        _write_bytes(self.ADDR, 0x05, 0)
        _write_bytes(self.ADDR, 0x06, 0)
        _write_bytes(self.ADDR, 0x03, 0)
        _write_bytes(self.ADDR, 0x21, 0xFF)
        _write_bytes(self.ADDR, 0x18, 0x01)
        sleep(100)
        _write_bytes(self.ADDR, 0x07, 0x17)
        sleep(100)

    def defaultInit(self):
        #ENABLE UV reading
        #these reg must be set to the fixed value
        _write_bytes(self.ADDR, 0x13, 0x29)
        _write_bytes(self.ADDR, 0x14, 0x89)
        _write_bytes(self.ADDR, 0x15, 0x02)
        _write_bytes(self.ADDR, 0x16, 0x02)

        self.writeParamData(0x01, 0x80 | 0x20 | 0x10 | 0x01)
        
        #set LED1 CURRENT(22.4mA)(It is a normal value for many LED)
        self.writeParamData(0x07, 0x03)
        _write_bytes(self.ADDR, 0x0F, 0x03)
        self.writeParamData(0x02, 0x01)
        
        #PS ADC SETTING
        self.writeParamData(0x0B, 0x00)
        self.writeParamData(0x0A, 0x07)
        self.writeParamData(0x0C, 0x20 | 0x20)

        #VIS ADC SETTING
        self.writeParamData(0x11, 0x00)
        self.writeParamData(0x10, 0x07)
        self.writeParamData(0x12, 0x20)
        
        #IR ADC SETTING
        self.writeParamData(0x1E, 0x00)
        self.writeParamData(0x1D, 0x07)
        self.writeParamData(0x1F, 0x20)

        #interrupt enable
        _write_bytes(self.ADDR, 0x03, 0x01)
        _write_bytes(self.ADDR, 0x04, 0x01)

        #AUTO RUN
        _write_bytes(self.ADDR, 0x08, 0xFF)
        _write_bytes(self.ADDR, 0x18, 0x0F)

    def readVisible(self):
        data = _read_bytes(self.ADDR, 0x22, 2)
        return data

    def readIR(self):
        data = _read_bytes(self.ADDR, 0x24, 2)
        return data
    
    def readUV(self):
        data = _read_bytes(self.ADDR, 0x2C, 2)
        return data

#     def readProximity(self, PSn):
#         data = _read_bytes(self.ADDR, PSn, 2)
#         return data

    def writeParamData(self, reg, value):
        #write Value into PARAMWR reg first
        _write_bytes(self.ADDR, 0x17, value)
        _write_bytes(self.ADDR, 0x18, reg | 0xA0)
        #SI114X writes value out to PARAM_RD,read and confirm its right
        data = _read_bytes(self.ADDR, 0x2E, 1)
        return data[0]

#     def firmware_version(self):
#         v = _read_bytes(self.ADDR, _CMD_TEST_GET_VER, 10)
#         version = v[6] - ord('0')
#         version = version * 10 + (v[8] - ord('0'))
#         return version
# 
#     def set_mode(self, enable):
#         if enable:
#             command = _CMD_EVENT_DET_MODE
#         else:
#             command = _CMD_BLOCK_DET_MODE
#         _write_bytes(self.ADDR, command, None)
# 
#     def read(self):
#         v = _read_bytes(self.ADDR, _CMD_GET_DEV_EVENT, 10)
#         return (v[4], v[5], v[6], v[7], v[8], v[9])

def _read_bytes(address, command, length):
    i2c.write(address, bytearray([command]))
    return i2c.read(address, length)

def _write_bytes(address, command, data=None):
    if data == None:
        i2c.write(address, bytearray([command]))
    else:
        i2c.write(address, bytearray([command, data]))
