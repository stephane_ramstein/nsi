# Add your Python code here. E.g.
from microbit import *
import tcs3472

t = tcs3472.tcs3472()

r, g, b = t.rgb()
l = t.light()
br = t.brightness()
print(r, g, b, l, br)