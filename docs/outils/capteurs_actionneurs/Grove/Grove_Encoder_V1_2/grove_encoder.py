class Encoder:
    def __init__(self, pin_sA, pin_sB):
        self.pin_sA = pin_sA
        self.pin_sB = pin_sB
        self.value = 0
        self.state = [self.pin_sA.read_digital(), self.pin_sB.read_digital()]
        self.direction = None
        self.new_state = [self.pin_sA.read_digital(), self.pin_sB.read_digital()]

    def update(self):
        self.new_state = [self.pin_sA.read_digital(), self.pin_sB.read_digital()]
        if self.new_state != self.state:
            if self.state == [0, 0]: # Resting position
                if self.new_state == [0, 1]: # Turned right 1
                    self.direction = "R"
                elif self.new_state == [1, 0]: # Turned left 1
                    self.direction = "L"

            elif self.state == [0, 1]: # R1 or L3 position
                if self.new_state == [1, 1]: # Turned right 1
                    self.direction = "R"
                elif self.new_state == [0, 0]: # Turned left 1
                    if self.direction == "L":
                        self.value = self.value - 1

            elif self.state == [1, 0]: # R3 or L1
                if self.new_state == [1, 1]: # Turned left 1
                    self.direction = "L"
                elif self.new_state == [0, 0]: # Turned right 1
                    if self.direction == "R":
                        self.value = self.value + 1

            else: # self.state == "11"
                if self.new_state == [0, 1]: # Turned left 1
                    self.direction = "L"
                elif self.new_state == [1, 0]: # Turned right 1
                    self.direction = "R"
                elif self.new_state == [0, 0]: # Skipped an intermediate 01 or 10 state, but if we know direction then a turn is complete
                    if self.direction == "L":
                        self.value = self.value - 1
                    elif self.direction == "R":
                        self.value = self.value + 1

            self.state = self.new_state
            
    def get(self):
        return self.direction, self.value
