from microbit import *
from my9221 import MY9221

ledbar = MY9221(di=pin0, dcki=pin14, reverse=False)

for i in range(1, 11):
    ledbar.level(i)
    sleep(500)
