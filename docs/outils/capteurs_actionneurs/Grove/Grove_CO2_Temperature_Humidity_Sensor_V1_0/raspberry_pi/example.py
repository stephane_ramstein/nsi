from grove_co2_scd30 import GroveCo2Scd30
import time

sensor = GroveCo2Scd30()

while True:
    if sensor.get_data_ready_status():
        co2, temperature, humidity = sensor.read()
        print(f"CO2 concentration is {co2:.1f} ppm")
        print(f"Temperature in Celsius is {temperature:.2f} C")
        print(f"Relative Humidity is {humidity:.2f} %")

    time.sleep(1)
