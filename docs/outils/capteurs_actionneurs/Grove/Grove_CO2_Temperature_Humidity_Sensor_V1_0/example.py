from microbit import *
import scd30

scd = scd30.SCD30(0x61)

# Wait for sensor data to be ready to read (by default every 2 seconds)
while scd.get_status_ready() != 1:
    sleep(200)

values = scd.read_measurement()
print(values)
