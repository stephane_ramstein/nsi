import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(12, GPIO.OUT)

pwm12 = GPIO.PWM(12, 5000)

pwm12.start(50)

input('Press return to stop:')	#Wait

pwm12.stop()
