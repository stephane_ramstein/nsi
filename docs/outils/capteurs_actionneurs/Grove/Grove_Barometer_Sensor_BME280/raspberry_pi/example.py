import bme280

bme280.setup() # setup probe connection
t = bme280.readTempC() # needed to read pressure
p = bme280.readFloatPressure()
h = bme280.readFloatHumidity()
print(t, p, h)
  
  # °C temp     : bme280.readTempC()
  # °F temp     : bme280.readTempF()
  # % humidity  : bme280.readFloatHumidity()
  # Pa pressure : bme280.readFloatPressure()
  # m alti      : bme280.readFloatAltitudeMeters()
  # feet alti   : bme280.readFloatAltitudeFeet()
  # set reference for altitude calculation :bme280.setReferencePressure(bme280.readFloatPressure()) 