from microbit import *
import bme280

bme280.BME280_I2C_ADDR = 0x76

b = bme280.BME280()

while True:
    print(b.get())
    sleep(1000)