## Written for micropython/microbit
##
## Author : Stephane Ramstein
## Date : 09/03/2023
##
## Should work with other ESP8285
##
## To use it:
## import esp8285


from microbit import *

HEAD = '''HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\n\r\n'''

def send_command(n, command):
    display.show(n)
    data = ''
    while 'OK' not in data:
        uart.write(command + '\r\n')
        data = ''
        while uart.any():
            data = data + str(uart.read())

    return data

def init(ssid, pwd):    
    uart.init(baudrate=115200, tx=pin14, rx=pin0)
    send_command('7', 'AT+RESTORE')
    send_command('6', 'AT+RST')
    send_command('5', 'AT+CWMODE=1')
    send_command('4', 'AT+RST')
    send_command('3', 'AT+CWJAP="' + ssid + '","' + pwd + '"')
    send_command('2', 'AT+CIPMUX=1')
    send_command('1', 'AT+CIPSERVER=1,80')

def get_request():
    # +IPD,0,467:GET /xxx HTTP/1.1
    data = None
    connection_id = None
    while data == None:
        data = str(uart.readline())
        if '+IPD' in data and 'GET' in data and 'HTTP' in data:
            left, right = data.split(':')
            data = right.split('GET ')[1].split(' HTTP')[0]
            connection_id = left.split('+IPD,')[1].split(',')[0]
        else:
            data = None
    return data, connection_id

def get_ip():
    data = send_command('', 'AT+CIFSR')
    ip_string = str(data).split('+CIFSR:STAIP,"')[1].split('"')[0]

    return ip_string

def send_data(data, connection_id):
    data = HEAD + data
    uart.write('AT+CIPSEND=' + str(connection_id) + ',' + str(len(data)) + '\r\n')
    sleep(1000)
    uart.write(data)

def load_page(file):
    fichier = open(file, 'r')
    page = fichier.read()
    fichier.close()
    return page