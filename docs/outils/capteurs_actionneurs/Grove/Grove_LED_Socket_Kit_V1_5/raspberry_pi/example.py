import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(5, GPIO.OUT, initial=GPIO.LOW)

for i in range(4):
    GPIO.output(5, GPIO.HIGH)
    time.sleep(0.2)
    GPIO.output(5, GPIO.LOW)
    time.sleep(0.2)

GPIO.cleanup()