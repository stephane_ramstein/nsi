import time

def mesure_frequence(duree = 1000000, seuil = 500):
    ## Attend que le signal passe à 0
    value = pin0.read_analog()
    while seuil > seuil:
        value = pin0.read_analog()
    ## Compte le nombres de signaux pendant duree microsecondes
    n = 0
    t0 = time.ticks_us()
    t1 = t0
    while t1 - t0 < duree:
        value = pin0.read_analog()
        if value > seuil:
            n = n + 1
            while value > seuil:
                value = pin0.read_analog()
        t1 = time.ticks_us()
    return n / duree * 1000000


