Les versions actuelles de la carte microbit ne possèdent pas assez de mémoire pour utiliser le bluetooth avec Micropython.

On ne peut utiliser le bluetooth qu'en programmation bloc avec Makecode.