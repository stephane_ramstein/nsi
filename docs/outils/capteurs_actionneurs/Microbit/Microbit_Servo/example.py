from microbit import * 

# brancher les piles
# certains servomoteurs ne fonctionnent bien qu'en alimentation 5V
# valeurs entre 1 et 100
# tester les valeurs limites par rapport au mouvement du servomoteur utilisé

pin0.set_analog_period(20)

while True: 
	pin0.write_analog(1)
	sleep(2000)
	pin0.write_analog(100)
	sleep(2000)
