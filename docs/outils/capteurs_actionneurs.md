# Ressources NSI - Outils


## Quelques capteurs et actionneurs pour Micro:bit et Raspberry

### Capteurs :

- Accéléromètre de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Accelerometer/microbits_Accelerometer.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Accelerometer/microbits_Accelerometer.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Accelerometer/example.py)
- Microswitches de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Buttons/microbits_buttons.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Buttons/microbits_buttons.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Buttons/example.py)
- Magnétomètre de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Compass/microbits_Compass.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Compass/microbits_Compass.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Compass/example.py)
- Capteur de lumière de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Light_Sensor/microbits_Light_Sensor.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Light_Sensor/microbits_Light_Sensor.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Light_Sensor/example.py)
- Capteur de température de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Temperature/microbits_Temperature.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Temperature/microbits_Temperature.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Temperature/example.py)

<br />

- Capteur de pression, humidité et température BME280 Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Barometer_Sensor_BME280/Grove_Barometer_Sensor_BME280.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Barometer_Sensor_BME280/Grove_Barometer_Sensor_BME280.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Barometer_Sensor_BME280/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_Barometer_Sensor_BME280/bme280.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Barometer_Sensor_BME280/raspberry_pi/)
- Capteur de CO2, température et humidité SCD30 Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_CO2_Temperature_Humidity_Sensor_V1_0/Grove_CO2_Temperature_Humidity_Sensor_V1_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_CO2_Temperature_Humidity_Sensor_V1_0/Grove_CO2_Temperature_Humidity_Sensor_V1_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_CO2_Temperature_Humidity_Sensor_V1_0/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_CO2_Temperature_Humidity_Sensor_V1_0/scd30.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_CO2_Temperature_Humidity_Sensor_V1_0/raspberry_pi/) (nécessite résistances et condensateurs de filtrage)
- Capteur de pression MPX5700AP Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Integrated_Pressure_Sensor_V1_0/Grove_Integrated_Pressure_Sensor_V1_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Integrated_Pressure_Sensor_V1_0/Grove_Integrated_Pressure_Sensor_V1_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Integrated_Pressure_Sensor_V1_0/example.py)
- Capteur de lumière Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Light_Sensor_V1_2/Grove_Light_Sensor_V1_2.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Light_Sensor_V1_2/Grove_Light_Sensor_V1_2.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Light_Sensor_V1_2/example.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Light_Sensor_V1_2/raspberry_pi/)
- Microswitch Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Micro_Switch_V1_0/Grove_Micro_Switch_V1_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Micro_Switch_V1_0/Grove_Micro_Switch_V1_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Micro_Switch_V1_0/example.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Micro_Switch_V1_0/raspberry_pi/)
- Potentiomètre circulaire, capteur d'angle Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Rotary_Angle_Sensor_V1_2/Grove_Rotary_Angle_Sensor_V1_2.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Rotary_Angle_Sensor_V1_2/Grove_Rotary_Angle_Sensor_V1_2.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Rotary_Angle_Sensor_V1_2/example.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Rotary_Angle_Sensor_V1_2/raspberry_pi/)
- Potentiomètre linéaire, capteur de déplacement linéaire Grove :
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Sliding_Potentiometer_V1_0/raspberry_pi/)
- Capteur de distance par ultrasons Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_Ultrasonic_Distance_Sensor_V2_0_and_HC-SR04/grove_ultrasonic_ranger.py)
- Capteur de couleur Grove :
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_I2C_Color_Sensor_V2_0/raspberry_pi/)
- Capteur d'humidité du sol Grove :
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Capacitive_Moisture_Sensor_V1_0/raspberry_pi/)
- Capteur de température DS18B20 Grove :
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_One_Wire_Temperature_Sensor_DS18B20/raspberry_pi/)

### Actionneurs :
- Matrice LED de la carte micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_LED_Display/microbits_LED_Display.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_LED_Display/microbits_LED_Display.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_LED_Display/example.py)

<br />

- Servomoteur
    - micro:bit : [pdf](./capteurs_actionneurs/Microbit/Microbit_Servo/microbit_Servo.pdf) ou [odt](./capteurs_actionneurs/Microbit/Microbit_Servo/microbit_Servo.odt), [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Servo/example.py)
- Afficheur 4 nombres à 7 segments Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_4-Digit_Display_V1_0/Grove_4-Digit_Display_V1_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_4-Digit_Display_V1_0/Grove_4-Digit_Display_V1_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_4-Digit_Display_V1_0/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_4-Digit_Display_V1_0/tm1637.py)
- Ecran LCD 16x2 :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_16x2_LCD_V2_0/Grove_16x2_LCD_V2_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_16x2_LCD_V2_0/Grove_16x2_LCD_V2_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_16x2_LCD_V2_0/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_16x2_LCD_V2_0/i2c_lcd.py)
- Barre de LEDs Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_LED_Bar_V2_0/Grove_LED_Bar_V2_0.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_LED_Bar_V2_0/Grove_LED_Bar_V2_0.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_LED_Bar_V2_0/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_LED_Bar_V2_0/my9221.py)
- LED Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_LED_Socket_Kit_V1_5/Grove_LED_Socket_Kit_V1_5.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_LED_Socket_Kit_V1_5/Grove_LED_Socket_Kit_V1_5.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_LED_Socket_Kit_V1_5/example.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_LED_Socket_Kit_V1_5/raspberry_pi/)
- Ruban LEDs de couleurs Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_RGB_LED_Strip/Grove_RGB_LED_Strip.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_RGB_LED_Strip/Grove_RGB_LED_Strip.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_RGB_LED_Strip/example.py)
- Haut parleur amplifié Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_Speaker_V1_1/Grove_Speaker_V1_1.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_Speaker_V1_1/Grove_Speaker_V1_1.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_Speaker_V1_1/example.py)
    - raspberry PI : [depot](https://gitlab.com/stephane_ramstein/nsi/-/tree/master/docs/outils/capteurs_actionneurs/Grove/Grove_Speaker_V1_1/raspberry_pi/) (nécessite résistances et condensateurs de filtrage)


### Dispositifs de communication :

- Bluetooth de la carte micro:bit : [md](./capteurs_actionneurs/Microbit/Microbit_Bluetooth/readme.md)
- Radio de la carte micro:bit : [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Radio/example.py)
- UART de la carte micro:bit : [exemple Python](./capteurs_actionneurs/Microbit/Microbit_Serial_via_pins_or_USB/example_uart_via_usb.py)

<br />

- Wifi ESP8285 Grove :
    - micro:bit : [pdf](./capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/Grove_UART_Wifi_V2.pdf) ou [odt](./capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/Grove_UART_Wifi_V2.odt), [exemple Python](./capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/example.py), [module Python](./capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/esp8285.py), [page web](./capteurs_actionneurs/Grove/Grove_UART_Wifi_V2/index.html)


### Systèmes embarqués :

- Robot Maqueen pour carte micro:bit : [pdf](./capteurs_actionneurs/Autres/DFRobot_maqueen/DFRobot_maqueen.pdf) ou [odt](./capteurs_actionneurs/Autres/DFRobot_maqueen/DFRobot_maqueen.odt), [exemple Python](./capteurs_actionneurs/Autres/DFRobot_maqueen/example.py), [module Python](./capteurs_actionneurs/Autres/DFRobot_maqueen/maqueen.py)

