# Ressources NSI - Outils


## Un serveur python

* Fiche explicative : [pdf](./serveur_python/07_Fiche_eleve_Serveur_python.pdf) ou [odt](./serveur_python/07_Fiche_eleve_Serveur_python.odt)
    * Ficher python de configuration du serveur **python_web_server_config.py** : [py](./serveur_python/serveur_python/python_web_server_config.py) (click droit - enregistrer la cible du lien sous)
    * Serveur python **python_web_server.py** : [py](./serveur_python/serveur_python/python_web_server.py) (click droit - enregistrer la cible du lien sous)
    * Page web dynamique modèle **index.html** : [html](./serveur_python/serveur_python/index.html) (click droit - enregistrer la cible du lien sous - renommer le fichier en index.html)
    * Fichier python **index.py** : [py](./serveur_python/serveur_python/index.py) (click droit - enregistrer la cible du lien sous)
* Les quatre fichiers compressés : [zip](./serveur_python/serveur_python.zip) (click droit - enregistrer la cible du lien sous)
