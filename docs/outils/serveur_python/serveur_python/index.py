#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Fichier index du site web dynamique.
'''

## importation des modules
import cgi


## Déclaration des fonctions

def charge_page_html(chemin):
    '''
    Charge le fichier HTML à envoyer au client.
    '''
    fichier_html = open(chemin, 'r', encoding='utf-8')
    page_html = fichier_html.read()
    fichier_html.close()
    
    return page_html

def envoie_page_html(page_html):
    '''
    Envoie le fichier HTML au client.
    '''
    print("Content-Type: text/html")    # HTML is following
    print()                             # blank line, end of headers
    print(page_html)


## Fonction principale

def main():
    # Récupération des données de formulaire
    form = cgi.FieldStorage()
    information = form.getvalue("information")

    # Chargement de la page html modèle
    page_html = charge_page_html('index.html')
    
    # Modification de la page html
    page_html = page_html.replace('$information$', str(information))
        
    # Modification du debug
#     page_html = page_html.replace('<!-- INSERER_DEBUG -->', 'toto')

    # Envoie de la page au client
    envoie_page_html(page_html)
    

## Programme principal
if __name__ == '__main__':
    main()
