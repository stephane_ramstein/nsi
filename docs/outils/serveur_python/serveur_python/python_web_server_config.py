#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Fichier de configuration du serveur Python
Adresse, port

Ce fichier sera modifié par les élèves

Ecrit par Stéphane Ramstein
stephane.ramstein@ac-lille.fr
'''

## Configuration de l'adresse et du port du serveur
SERVER_ADDRESS = '127.0.0.1' # '' ou 127.0.0.1 pour localhost
PORT = 8000 # souvent 80, 8080, 8000 pour un serveur web
