#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Serveur web Python

Ce fichier ne sera à priori ni ouvert, ni modifié par les élèves ou l'enseignant

Sa configuration (adresse, port) se fait à travers le fichier python_server_web_config.py

Ecrit par Stéphane Ramstein
stephane.ramstein@ac-lille.fr
"""

import http.server
import python_web_server_config # Importation du module de configuration du serveur modifié par les élèves

def main():
    server = http.server.HTTPServer
    handler = http.server.CGIHTTPRequestHandler
    handler.cgi_directories = ["/"]

    httpd = server((python_web_server_config.SERVER_ADDRESS, python_web_server_config.PORT), handler) # Initialisation du serveur HTTP

    print('Démarrage du serveur httpd...')
    print("Serveur", python_web_server_config.SERVER_ADDRESS, "actif sur le port :", python_web_server_config.PORT)
    print('CTRL-C pour arrêter le serveur ...')

    try:
        httpd.serve_forever() # Lancement du serveur
    except:
        pass

    print('Arrêt du serveur httpd...')
    httpd.server_close() # Fermeture du serveur

if __name__ == '__main__':
    main()
