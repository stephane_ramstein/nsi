# Ressources NSI - Outils


## Logiciels

Le tableau ci-dessous regroupe quelques logiciels utiles pour apprendre la NSI et leur méthode d'installation pour les trois systèmes d'exploitation classiques.

<table>
    <tr>
        <td></td>
        <td align="center"><b>Windows</b></td>
        <td align="center"><b>Linux</b></td>
        <td align="center"><b>MacOS</b></td>
    </tr>
    <tr>
        <td>Langage de programmation <a href="https://www.python.org/">Python</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
        <td align="center"><a href="https://www.python.org/downloads/">à installer</a></td>
    </tr>
    <tr>
        <td>Editeur de programmes Python <a href="https://thonny.org/">Thonny</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
        <td align="center"><a href="https://thonny.org/">à installer</a></td>
    </tr>
    <tr>
        <td>Emulateur Linux <a href="https://www.cygwin.com/">Cygwin</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/cygwin.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Système de gestion de base de données <a href="https://www.sqlite.org/index.html">SQLite</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/sqlite-tools-windows.zip">à décompresser</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/sqlite-tools-linux.zip">à décompresser</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/sqlite-tools-osx.zip">à décompresser</a></td>
    </tr>
    <tr>
        <td>Simulateur de processeur <a href="http://rmdiscala.free.fr/informatique/index.html">PicoProcesseur</a> de RM di Scala</td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/PicoProcesseur.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Analyseur de protocoles <a href="https://www.wireshark.org">Wireshark</a></td>
        <td align="center"><a href="https://www.wireshark.org/download.html">à décompresser ou à installer</a></td>
        <td align="center"><a href="https://www.wireshark.org/download.html">à décompresser ou à installer</a></td>
        <td align="center"><a href="https://www.wireshark.org/download.html">à décompresser ou à installer</a></td>
    </tr>
    <tr>
        <td>Driver de capture de paquets pour Windows <a href="https://nmap.org/npcap/">Npcap</a></td>
        <td align="center"><a href="https://nmap.org/npcap/">à installer si nécessaire</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Editeur de programme pour carte Micro:Bit <a href="https://python.microbit.org">Python Editor</a></td>
        <td align="center"><a href="https://python.microbit.org">en ligne</a></td>
        <td align="center"><a href="https://python.microbit.org">en ligne</a></td>
        <td align="center"><a href="https://python.microbit.org">en ligne</a></td>
    </tr>
    <tr>
        <td>Editeur de programme pour carte Micro:Bit <a href="https://codewith.mu/">Mu Editor</a></td>
        <td align="center"><a href="https://codewith.mu/en/download">à installer</a></td>
        <td align="center"><a href="https://codewith.mu/en/download">à installer</a></td>
        <td align="center"><a href="https://codewith.mu/en/download">à installer</a></td>
    </tr>
    <tr>
        <td>Editeur de programme Notepad++ <a href="https://notepad-plus-plus.org">Notepad++</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/npp.8.3.2.portable.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Logiciel de conception de circuits logiques <a href="https://logisim.altervista.org/">Logisim</a></td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/Logisim-ITA.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Logiciel de conception de circuits logiques <a href="https://circuitverse.org/simulator">CircuitVerse</a></td>
        <td align="center"><a href="https://circuitverse.org/simulator">en ligne</a></td>
        <td align="center"><a href="https://circuitverse.org/simulator">en ligne</a></td>
        <td align="center"><a href="https://circuitverse.org/simulator">en ligne</a></td>
    </tr>
    <tr>
        <td>Logiciel de conception de réseaux <a href="https://www.lernsoftware-filius.de/Herunterladen">Filius</a></td>
        <td align="center"><a href="https://www.lernsoftware-filius.de/Herunterladen">à installer</a></td>
        <td align="center"><a href="https://www.lernsoftware-filius.de/Herunterladen">à installer</a></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Editeur de markdown StackEdit <a href="https://stackedit.io/">StackEdit</a></td>
        <td align="center"><a href="https://stackedit.io/">en ligne</a></td>
        <td align="center"><a href="https://stackedit.io/">en ligne</a></td>
        <td align="center"><a href="https://stackedit.io/">en ligne</a></td>
    </tr>
    <tr>
        <td>Editeur de programme Notepad++ avec l'extension MarkdownViewer ++</td>
        <td align="center"><a href="https://gitlab.com/stephane_ramstein/nsi/-/blob/master/docs/outils/logiciels/notepad++_with_markdown.zip">à décompresser</a></td>
        <td align="center"></td>
        <td align="center"></td>
    </tr>
    <tr>
        <td>Editeur de markdown Ghostwriter <a href="https://wereturtle.github.io/ghostwriter/">Ghostwriter</a></td>
        <td align="center"><a href="https://wereturtle.github.io/ghostwriter/">à installer</a></td>
        <td align="center"><a href="https://wereturtle.github.io/ghostwriter/">à installer</a></td>
        <td align="center"><a href="https://wereturtle.github.io/ghostwriter/">à installer</a></td>
    </tr>
</table>
