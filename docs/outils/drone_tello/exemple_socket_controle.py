import socket, time

locaddr = (host,port)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
tello_address = ('192.168.10.1', 8889)
sock.bind(locaddr)

sock.sendto('command'.encode(encoding="utf-8"), tello_address)
print('command', sock.recvfrom(1024))

sock.sendto('battery?'.encode(encoding="utf-8"), tello_address)
print('battery?', sock.recvfrom(1024))

sock.sendto('takeoff'.encode(encoding="utf-8"), tello_address) # Takeoff
print('takeoff', sock.recvfrom(1024))
time.sleep(5)

sock.sendto('cw 360'.encode(encoding="utf-8"), tello_address) # Rotate clockwise 360
print('cw 360', sock.recvfrom(1024))
time.sleep(5)

sock.sendto('land'.encode(encoding="utf-8"), tello_address) # Land
print('land', sock.recvfrom(1024))
time.sleep(5)