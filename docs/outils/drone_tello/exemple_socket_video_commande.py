# Ce programme permet de démarrer le flux video
# Le flux est arrêté au bout de 60 secondes
# Les commandes doivent être envoyées au drone via son adresse IP sur le port 8889
# Le flux vidéo doit être récupéré par udp à l'adresse 0.0.0.0 sur le port 11111 (udp://@0.0.0.0:11111)
# Il faut donc un deuxième programme s'exécutant dans un thread à part pour récupérer le flux vidéo
# Voir exemple_socket_video_visualisation.py

import socket, time

locaddr = (host,port)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
tello_address = ('192.168.10.1', 8889)
sock.bind(locaddr)

sock.sendto('command'.encode(encoding="utf-8"), tello_address)
print('command', sock.recvfrom(1024))
sock.sendto('battery?'.encode(encoding="utf-8"), tello_address)
print('battery?', sock.recvfrom(1024))

print('Stream on')
sock.sendto('streamon'.encode(encoding="utf-8"), tello_address)
print(sock.recvfrom(1024))
time.sleep(60)
sock.sendto('streamoff'.encode(encoding="utf-8"), tello_address)
print(sock.recvfrom(1024))
print('Stream off')
