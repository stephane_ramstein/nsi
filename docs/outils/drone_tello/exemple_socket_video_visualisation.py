import cv2
import time

udp_address = 'udp://@0.0.0.0:11111'

tello_video = cv2.VideoCapture(udp_address)

if not tello_video.isOpened():
    tello_video.open(udp_address)

success = True

while success:
    success, frame = tello_video.read()
    cv2.imshow("drone", frame)
    cv2.waitKey(1)
