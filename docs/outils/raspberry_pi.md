# Ressources NSI - Outils


## Le raspberry PI

- Préparer un raspberry PI : [pdf](./raspberry_pi/02_preparer_un_raspberry_pi.pdf) ou [odt](./raspberry_pi/02_preparer_un_raspberry_pi.odt) **(à mettre à jour)**
- Le port GPIO  : [pdf](./raspberry_pi/03b_Fiche_eleve_Raspberry_GPIO.pdf) ou [odt](./raspberry_pi/03b_Fiche_eleve_Raspberry_GPIO.odt)
- Compléments sur le port GPIO  : [pdf](./raspberry_pi/03c_Fiche_eleve_Raspberry_GPIO_compléments.pdf) ou [odt](./raspberry_pi/03c_Fiche_eleve_Raspberry_GPIO_compléments.odt)
- Le Grove Base Hat et son Convertisseur Analogique Numérique : [pdf](./raspberry_pi/03e_Fiche_eleve_Grove_Base_Hat.pdf) ou [odt](./raspberry_pi/03e_Fiche_eleve_Grove_Base_Hat.odt)
- Rendre autonome un raspberry PI : [pdf](./raspberry_pi/05_rapsberry_systeme_embarque.pdf) ou [odt](./raspberry_pi/05_rapsberry_systeme_embarque.odt)


