# Ressources NSI - Outils


## Prise du microcontrôleur micro:bit

Un microcontrôleur est un processeur associé à une mémoire, pour stocker programmes et données, et d'entrées/sorties compatibles avec la plupart des circuits électroniques. Ils sont utilisés pour commander des actionneurs, acquérir les données de capteurs ou plus généralement piloter les circuits électroniques.

1\. Installer l'environnement de programmation Mu Editor ou utiliser la version en ligne : [md](../outils/logiciels.md) ou [lien](https://python.microbit.org/)
    - Fiche sur l'éditeur Mu Editor :: [odt](./initiation_microbit/Python_editor_for_microbit.odt) ou [pdf](./initiation_microbit/Python_editor_for_microbit.pdf)

2\. Généralités sur la carte micro:bit : [odp](./initiation_microbit/initiation_MicroBit.odp) ou [pdf](./initiation_microbit/initiation_MicroBit.pdf)

3\. Fiche carte micro:bit : [odt](./initiation_microbit/109_Carte_MicroBit.odt) ou [pdf](./initiation_microbit/109_Carte_MicroBit.pdf)

4\. Faire les activités proposées dans l'éditeur en ligne ou dans les liens à la fin du diaporama

5\. Fiche acquisition analogique pour les sciences : [odt](./initiation_microbit/110_Carte_MicroBit_Acquisition_analogique.odt) ou [pdf](./initiation_microbit/110_Carte_MicroBit_Acquisition_analogique.pdf)

