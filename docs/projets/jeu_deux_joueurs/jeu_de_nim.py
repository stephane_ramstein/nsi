#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Ce module définit une abstraction pour les jeux à deux joueurs.
    Cette abstraction peut être utilisé dans un module `twoplayersgame`.

    Pour définir un jeu en particulier, vous devez choisir comment représenter une situation de jeu et un joueur
    et, ensuit, créer un mpodule qui implémentent les fonctions de ce module.
    
    JEU DE NIM

"""


def create_first_situation():
    """construit la situation initiale du jeu.
    create_first_situation() -> situation
    
    Dans le JEU DE NIM, situation sera le nombre d'allumettes.
    
    :returns: *(situation)* la situation initiale au début du jeu
    """
    
    nombre_allumettes = int(input("Entrez le nombre d'allumettes : "))
    
    return nombre_allumettes


def is_game_finished(current_situation, current_player):
    """
    indique si *current_situation* est une situation de fin de jeu quand c'est à *current_player* de jouer
    gameIsFinished(situation, player) -> boolean

    Dans le JEU DE NIM, le jeu se termine s'il reste 0 ou 1 allumette.

    :param current_situation: la situation couran
    :type situation: situation
    :param current_player: le joueur qui joue
    :type player: player
    :returns: *(boolean)* -- True si la situation courante termine le jeu
    """
    
    nombre_allumettes = current_situation
    
    if nombre_allumettes == 0 or nombre_allumettes == 1:
        return True
    else:
        return False


def player_can_play(current_situation, current_player):
    """
    indique si le joueur *current_player* peut jouer dans la situation *current_situation*
    player_can_play(current_situation, current_player) -> boolean

    Dans le JEU DE NIM, les joueurs peuvent toujours jouer si le jeu n'est pas terminé.

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(boolean)* -- True ssi le joueur peut jouer dans la situation donnée
    """
    
    return True


def next_situations(current_situation, current_player):
    """
    renvoie la liste des situations qui peuvent être atteintes à partir de  *current_situation* quand c'est à  *current_player* de jouer
    nextSituations(situation) -> List<Situation>

    Dans le JEU DE NIM c'est le nombre d'allumettes restantes moins 1, 2 ou 3 allumettes.
    Ce nombre doit être positif ou nul.

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(list<situtation>)* -- la liste des situations qui peuvent être atteintes en un coup par le joueur
    """

    nombre_allumettes = current_situation
    next_situations_list = ()
    for i in range(1, 4):
        situation = nombre_allumettes - i
        if situation >= 0:
            next_situations_list = next_situations_list + (situation,)
    
    return next_situations_list


def get_winner(current_situation, last_player, other_player):
    """
    renvoie le joueur vainqueur dans la situation  *current_situation* (supposée finale) quand *last_player*est le dernier joueur à avoir joué
    get_winner(current_situation, last_player, other_player) -> Player

    Dans le JEU DE NIM le vaiqueur est celui qui atteint la situation à 0 allumettes.

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui a joué en dernier
    :type player: player
    :param other_player: l'autre joueur
    :type player: player
    :returns: *(Player)* -- le vainqueur
    """
    
    nombre_allumettes = current_situation
    
    if nombre_allumettes == 0:
        return last_player
    else:
        return None


def display_game(current_situation):
    """
    affiche la situation *current_situation*
    display_game(current_situation) -> None
    
    Dans le JEU DE NIM, affiche le nombre d'allumettes restantes.
    
    :param current_situation: la situation courante
    :type situation: situation
    """
    
    nombre_allumettes = current_situation
    
    print('Il reste ', nombre_allumettes, ' allumettes.', sep='')


def human_player_plays(next_situations_list, human_player):
    """
    fait jouer un coupe au joueur humain à partir de la situation *current_situation*, la situation atteinte est renvoyée
    human_player_plays(current_situation, human_player) -> Situation
    
    Dans le JEU DE NIM, un joueur humain choisi uniquement une solution dans la liste.
    
    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(new_situation)* la situation atteinte après que le joueur humain ait joué un coup
    """
    
    choix = int(input('Entrez le numéro de la situation choisie : '))
    
    new_situation = next_situations_list[choix]
    
    return new_situation


# def human_player_plays(current_situation, human_player):
#     """
#     fait jouer un coupe au joueur humain à partir de la situation *current_situation*, la situation atteinte est renvoyée
#     human_player_plays(current_situation, human_player) -> Situation
#     
#     Dans le JEU DE NIM, un joueur humain peut tirer 1, 2 ou 3 allumettes s'il en reste assez.
#     
#     :param current_situation: la situation courante
#     :type situation: situation
#     :param current_player: le joueur qui doit jouer
#     :type player: player
#     :returns: *(new_situation)* la situation atteinte après que le joueur humain ait joué un coup
#     """
#     
#     nombre_allumettes = current_situation
#     
#     if nombre_allumettes >= 3:
#         choix_joueur = int(input(human_player + ', voulez-vous retirer 1, 2 ou 3 allumettes ? '))
#         new_situation = nombre_allumettes - choix_joueur
#     elif nombre_allumettes == 2:
#         choix_joueur = int(input(human_player + ', voulez-vous retirer 1 ou 2 allumettes ? '))
#         new_situation = nombre_allumettes - choix_joueur
# 
#     print()
#     
#     return new_situation


def eval_function(current_situation, current_player):
    """
    La fonction d'évaluation évalue la situation *current_situation* pour le joueur *current_player*
    La fonction d'évaluation est croissante avec la qualité de la situation pour le joueur.
    evalFunction(situation, player) -> number value

    Dans le JEU DE NIM s'il reste 0 allumette, le joueur a gagné.
    S'il reste 1 allumette, la partie est nulle.

    :param current_situation: la situation courante
    :type situation: situation
    :param current_player: le joueur qui doit jouer
    :type player: player
    :returns: *(number)* -- le score de la situation pour le joueur donné.
        Meilleure est la situation pour le joueur "min-Max", plus haut est le scole. La situation est inverse pour le joueur adverse (typiquement, le joueur humain).
    """
    
    nombre_allumettes = current_situation
    
    if nombre_allumettes == 0:
        return 3
    elif nombre_allumettes == 1:
        return 1
    elif nombre_allumettes == 2 or nombre_allumettes == 3:
        return 0
    else:
        return 2