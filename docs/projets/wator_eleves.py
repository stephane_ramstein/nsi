#!/usr/bin/python3
# -*- coding: utf-8 -*-


## Importation des modules

import random

import matplotlib
import matplotlib.pyplot as plt

import tkinter
import time


## Déclaration des constantes

GESTATION_THON = 2
GESTATION_REQUIN = 5
ENERGIE_REQUIN = 3
PROPORTION_THON = 30
PROPORTION_REQUIN = 10
HAUTEUR_GRILLE = 25
LARGEUR_GRILLE = 25
NOMBRE_ETAPES = HAUTEUR_GRILLE * LARGEUR_GRILLE * 200
FPS = 100000


## Définition des fonctions

def case_vide():
    '''
    Une case vide.
    '''
    return {'type': 'vide'}

def thon(gestation=GESTATION_THON):
    '''
    Un thon.
    '''
    return {'type': 'thon', 'gestation': gestation}

def requin(gestation=GESTATION_REQUIN, energie=ENERGIE_REQUIN):
    '''
    Un requin.
    '''
    return {'type': 'requin', 'gestation': gestation, 'energie': energie}

def liste_cases_type(grille, i_case, j_case, type_case):
    '''
    Liste les cases voisines d'un type donné.
    '''
    hauteur = len(grille)
    largeur = len(grille[0])
    liste_cases = []
    for (i, j) in [((i_case - 1) % hauteur, j_case), ((i_case + 1) % hauteur, j_case), (i_case, (j_case - 1) % largeur), (i_case, (j_case + 1) % largeur)]:
        if grille[i][j]['type'] == type_case:
            liste_cases.append((i, j))
    return liste_cases

def comportement_thon(grille, i_thon, j_thon):
    '''
    Gestion du comportement d'un thon.
    '''
    hauteur = len(grille)
    largeur = len(grille[0])
    grille[i_thon][j_thon]['gestation'] = grille[i_thon][j_thon]['gestation'] - 1 # perte d'un point de gestation
    cases_vides = liste_cases_type(grille, i_thon, j_thon, 'vide') # recensement des cases vides autour du thon
    if len(cases_vides) != 0: # s'il existe au moins une case vide
        (nouveau_i, nouveau_j) = random.choice(cases_vides) # on en choisit une au hasard
        grille[nouveau_i][nouveau_j] = grille[i_thon][j_thon] # le thon se déplace sur cette nouvelle case
        if grille[nouveau_i][nouveau_j]['gestation'] == 0: # si la gestation est nulle
            grille[nouveau_i][nouveau_j]['gestation'] = GESTATION_THON # réinitialisation de la gestation du thon
            grille[i_thon][j_thon] = thon() # création d'un nouveau thon sur l'ancienne case
        else: # sinon la gestation est non nulle
            grille[i_thon][j_thon] = case_vide() # l'ancienne case devient vide
    
    return grille

def comportement_requin(grille, i_requin, j_requin):
    '''
    Gestion du comportement d'un requin.
    '''
    pass
    
    return grille
    
def creer_grille(nbr_lignes, nbr_colonnes):
    '''
    Crée une grille avec des cases vides.
    '''
    return [[case_vide() for j in range(nbr_colonnes)] for i in range(nbr_lignes)]

def initialise_grille(grille):
    '''
    Remplit une grille avec des thons et des requins suivant les proportions paramétrées.
    '''
    hauteur = len(grille)
    largeur = len(grille[0])
    for i in range(hauteur):
        for j in range(largeur):
            nbr_aleat = random.randint(0, 100)
            if nbr_aleat < PROPORTION_REQUIN: 
                grille[i][j] = requin()
            elif PROPORTION_REQUIN <= nbr_aleat and nbr_aleat < PROPORTION_REQUIN + PROPORTION_THON:
                grille[i][j] = thon()
    return grille

def grille_en_texte(grille):
    '''
    Convertit une grille en une chaîne de caractères.
    '''
    texte = ''
    for ligne in grille:
        for element in ligne:
            if element['type'] == 'vide':
#                 texte = texte + chr(9634)
                texte = texte + chr(9898)
            elif element['type'] == 'thon':
#                 texte = texte + chr(9638)
                texte = texte + chr(9917)
            elif element['type'] == 'requin':
#                 texte = texte + chr(9632)
                texte = texte + chr(9899)
        texte = texte + '\n'
    return texte
        
def etape_suivante(grille):
    '''
    Choisit une case aléatoirement dans la grille et calcule l'état suivant de la grille en fonction du comportement de la case choisie.
    '''
    hauteur = len(grille)
    largeur = len(grille[0])
    i_case = random.randint(0, hauteur - 1)
    j_case = random.randint(0, hauteur - 1)
    if grille[i_case][j_case]['type'] == 'thon':
        grille = comportement_thon(grille, i_case, j_case)
    elif grille[i_case][j_case]['type'] == 'requin':
        grille = comportement_requin(grille, i_case, j_case)
    
    return grille

def compte_poissons(grille):
    '''
    Compte le nombre de thons et de requins présents sur la grille.
    '''
    hauteur = len(grille)
    largeur = len(grille[0])
    nbr_thons = 0
    nbr_requins = 0
    for i in range(hauteur):
        for j in range(largeur):
            if grille[i][j]['type'] == 'thon':
                nbr_thons = nbr_thons + 1
            elif grille[i][j]['type'] == 'requin':
                nbr_requins = nbr_requins + 1
    return (nbr_thons, nbr_requins)

def mise_a_jour_grille(zone_texte, grille):
    '''
    Met à jour la zone de texte de la fenêtre avec la grille.
    '''
    zone_texte.config(text=grille_en_texte(grille))

def simulation(zone_texte, grille, fps):
    '''
    Met à jour la fenêtre avec les états de la grille calculés successivement.
    Tant que la simulation est active.
    '''
    global continuer
    continuer = True
    num_etape = 0
    while continuer == True:
        if num_etape % (HAUTEUR_GRILLE * LARGEUR_GRILLE) == 0:
            mise_a_jour_grille(zone_texte, grille)
            zone_texte.update()
        grille = etape_suivante(grille)
#         time.sleep(1/fps)

def arreter_simulation():
    '''
    Permet d'arrêter la simulation.
    '''
    global continuer
    continuer = False

def wator_graphique():
    '''
    Evalue les évolutions des populations de thons et de requins et les affiche grâce à matplotlib.
    '''
    grille = creer_grille(HAUTEUR_GRILLE, LARGEUR_GRILLE)
    grille = initialise_grille(grille)

    t = range(NOMBRE_ETAPES)

    thons = []
    requins = []
    for x in t:
        (nbr_thons, nbr_requins) = compte_poissons(grille)
        thons.append(nbr_thons)
        requins.append(nbr_requins)
        grille = etape_suivante(grille)

    fig, ax = plt.subplots()
    courbe_1, = ax.plot(t, thons)
    courbe_2, = ax.plot(t, requins)

    ax.set(xlabel='Etapes', ylabel="Nombre de thons ou de requins",
           title='Evolution des populations de thons et de requins')
    ax.legend([courbe_1, courbe_2], ['thons', 'requins'])
    ax.grid()

    plt.show()

def wator_anime():
    '''
    Anime l'évolution de la grille avec le module tkinter.
    '''
    grille = creer_grille(HAUTEUR_GRILLE, LARGEUR_GRILLE)
    grille = initialise_grille(grille)

    fenetre = tkinter.Tk()
    fenetre.title('Planète Wator')
    zone_texte = tkinter.Label(font=('Noto Mono', 10))
    bouton_lancer_simulation = tkinter.Button(text='Lancer la simulation', command = lambda: simulation(zone_texte, grille, FPS))
    bouton_arreter_simulation = tkinter.Button(text='Arrêter la simulation', command = arreter_simulation)
    zone_texte.pack()
    bouton_lancer_simulation.pack()
    bouton_arreter_simulation.pack()

    mise_a_jour_grille(zone_texte, grille)
    zone_texte.update()

    continuer = True

    fenetre.mainloop()

def main():
    '''
    Fonction principale.
    Commence par afficher les évolutions des populations de thons et de requins puis anime l'évolution de la grille.
    '''
    print('Calculs en cours. Veuillez patienter.')
    wator_graphique()
    wator_anime()


## Programme principal
if __name__ == '__main__':
    main()



