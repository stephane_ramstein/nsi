# TP d'Initiation au Module NetworkX avec Python

## Objectif
L'objectif de ce TP est de modéliser visuellement des graphes avec le module NetworkX.

## Exercice 1: Création d'un graphe simple

1. Importez le module NetworkX et Matplotlib dans votre script Python.

>[!NOTE]
> ```python
> import networkx as nx
> import matplotlib.pyplot as plt
> ```

Pour créer un graphe avec NetworkX, on utilise la classe `nx.Graph()` pour un graphe non orienté ou `nx.DiGraph()` pour un graphe orienté.

On peut ensuite ajouter les sommets séparément et les relier par des arêtes ou créer directement les liens entre les sommets.

>[!CAUTION]
> Avec NetworkX :
> - Arête ou arc : `edge`
> - Sommet : `node`

2. Créez un graphe simple avec trois nœuds et deux arêtes.
3. Affichez le graphe généré avec `nx.draw()`.

>[!NOTE]
> ```python
> G = nx.Graph()
> G.add_edges_from([("A", "B"), ("B", "C")])
> nx.draw(G, with_labels=True)
> plt.show()
> ```

## Exercice 2: Personnalisation du graphe

On peut ajouter des attributs aux sommets et aux arêtes pour ajouter des couleurs, des pondérations, changer la forme des sommets ou des arêtes, etc.

>[!CAUTION]
> - Forme : `node_shape` dans `nx.draw_networkx_nodes()`\
> - Couleur : `node_color` pour les sommets et `edge_color` pour les arêtes\
> - Pondération : `weight` et `labels`

Vous trouverez les options de personnalisation des graphes dans la documentation NetworkX : [NetworkX Documentation](https://networkx.org/documentation/stable/reference/drawing.html).

Reproduire le graphe suivant en utilisant `node_color`, `edge_color`, et `labels`.

<img src="img/TP1.png">


<br>
<br>

>[!NOTE]
> ```python
> G = nx.Graph()
> G.add_edges_from([("A", "B"), ("B", "C"), ("C", "D")])
>
> pos = nx.spring_layout(G)
> nx.draw(G, pos, with_labels=True, node_color="lightblue", edge_color="gray", node_size=2000, font_size=15)
>
> plt.show()
> ```

