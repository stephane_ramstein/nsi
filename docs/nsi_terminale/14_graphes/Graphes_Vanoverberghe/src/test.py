import matplotlib.pyplot as plt
import networkx as nx

G = nx.cubical_graph()
pos = nx.spring_layout(G, seed=42)  # positions for all nodes

# nodes
options = {"edgecolors": "tab:gray", "node_size": 800, "alpha": 0.9}
nx.draw_networkx_nodes(G, pos, nodelist=[0, 1, 2, 3], node_color="tab:green", **options)
nx.draw_networkx_nodes(G, pos, nodelist=[4, 5, 6, 7], node_color="tab:purple", **options)

# edges
nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5)
nx.draw_networkx_edges(
    G,
    pos,
    edgelist=[(0, 1), (1, 2), (2, 3), (3, 0)],
    width=8,
    alpha=0.5,
    edge_color="tab:orange",
)
nx.draw_networkx_edges(
    G,
    pos,
    edgelist=[(4, 5), (5, 6), (6, 7), (7, 4)],
    width=8,
    alpha=0.5,
    edge_color="tab:pink",
)

# new labels
labels = {}
labels[0] = r"$X$"
labels[1] = r"$Y$"
labels[2] = r"$Z$"
labels[3] = r"$W$"
labels[4] = r"$M$"
labels[5] = r"$N$"
labels[6] = r"$O$"
labels[7] = r"$P$"
nx.draw_networkx_labels(G, pos, labels, font_size=22)

plt.tight_layout()
plt.axis("off")
plt.show()