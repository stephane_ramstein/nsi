M = [
        [1, 2, 3],
        [3, 2, 2],
        [2, 1, 4]
    ]

def carre(m):
    res = []
    size = len(m[0])
    for i in range(size):
        tmp = []
        for j in range(size):
            val = 0
            for k in range(size):
                val = val + m[k][j] * m[i][k]
            tmp.append(val)
        res.append(tmp)
    return res

M_2 = carre(M)

print(M_2)
