# Importation des modules 


# Déclaration de la classe

class Graphe:
	
	def __init__(self):
		self.liste_adjacents = {}
		self.valeur = {}

	def voisins(self,x):
		assert x in self.liste_adjacents, ("Le sommet n'existe pas")
		return self.liste_adjacents[x]
	
	def sont_adjacent(self,x,y):
		assert x in self.liste_adjacents and y in self.liste_adjacents, ("Un des sommets n'est pas dans le graphe")
		if y in self.liste_adjacents[x] and x in self.liste_adjacents[y]:
					return True
		else :
					return False
					
	def ajouter_sommet(self,x):
		assert not x in self.liste_adjacents, ("Sommet déja existant")
		self.liste_adjacents[x] = []
		
	def ajouter_arrete(self,x,y):
		 assert x in self.liste_adjacents and y in self.liste_adjacents, ("Un des sommets n'est pas dans le graphe") 
		 self.liste_adjacents[x].append(y)
		 self.liste_adjacents[y].append(x)
		
	def supprimer_arrete(self,x,y):
		assert self.sont_adjacent(x,y),("Il n'y a pas d'arrete entre les deux sommets")
		indy = self.liste_adjacents[x].index(y)
		indx = self.liste_adjacents[y].index(x)
		del self.liste_adjacents[x][indy]
		del self.liste_adjacents[y][indx]
		
			  	  	  
	def supprimer_sommet(self,x):
		assert x in self.liste_adjacents, ("Le sommet n'est pas dans le graphe")
		for sommet in self.liste_adjacents[x]:
			self.supprimer_arrete(x,sommet)
		del self.liste_adjacents[x]
			 
	def retourner_valeur_sommet(self,x):
		if not x in self.valeur:
			return None
		else :
			return self.valeur[x]
		
	def fixer_valeur_sommet(self,x,val):
		assert x in self.liste_adjacents, ("Le sommet n'est pas dans le graphe")
		self.valeur[x] = val

#Programme principal
g = Graphe()
g.ajouter_sommet(1)
g.ajouter_sommet(2)
g.ajouter_arrete(1,2)
g.ajouter_sommet(3)
g.ajouter_arrete(1,3)
print(g.liste_adjacents)
print(g.voisins(1))
print(g.sont_adjacent(3,2))