class Graphe_oriente_mat() :
    '''
    une classe pour les graphes avec une matric adjacente
    '''
    def __init__(self, n):
        '''
        construit un graphe vide avec pour attributs :
        - n : la dimension de la matrice adjacente
        - adj : la matrice adjacente (une liste de listes) ne contenant que des "False"
        : param n dimension de la matrice adjacente
        type int
        : param noms_sommets 
        type liste
        '''
        'self.n= à compléter'
        pass
    
    
    
    def ajouter_arc(self, noeud1, noeud2):
        '''
        ajoute un arc orienté de noeud1 vers noeud2
        : param noeud1, noeud2 deux entiers entre 0 et n-1
        type int
        : pas de return, EFFET DE BORD sur self
        >>> g = Graphe_oriente_mat(4)
        >>> g.ajouter_arc(0, 1)
        >>> g.ajouter_arc(0, 3)
        >>> g.ajouter_arc(1, 2)
        >>> g.ajouter_arc(3, 1)
        >>> g.adj == [[False, True, False, True], [False, False, True, False], [False, False, False, False], [False, True, False, False]]
        True
        '''
        pass

    
    def a_arc(self, noeud1, noeud2) :
        '''
        renvoie True si il y a un arc orienté de noeud1 vers noeud2 et False sinon
         : param noeud1, noeud2 deux entiers entre 0 et n-1
        type int
        : return
        type boolean
        >>> g = Graphe_oriente_mat(4)
        >>> g.ajouter_arc(0, 1)
        >>> g.ajouter_arc(0, 3)
        >>> g.ajouter_arc(1, 2)
        >>> g.ajouter_arc(3, 1)
        >>> g.a_arc(0,1)
        True
        >>> g.a_arc(1, 3)
        False
        '''
        pass
    
    def voisins(self, noeud):
        '''
        renvoie les voisins du noeud
        : param noeud
        type int
        : return liste des voisins
        type list
        >>> g = Graphe_oriente_mat(4)
        >>> g.ajouter_arc(0, 1)
        >>> g.ajouter_arc(0, 3)
        >>> g.ajouter_arc(1, 2)
        >>> g.ajouter_arc(3, 1)
        >>> g.successeurs(1) == [2]
        True
        >>> g.voisins(0) == [1, 3]
        True
        >>> g.voisins(2) == []
        True
        
        '''
        pass
    
   
    
    def __str__(self):
        '''
        renvoie une chaîne pour l'affichage du graphe via un print
        '''
        chaine = ''
        for sommet in range("a compléter"):
            chaine += str("a compléter") + ' -> '
            #for voisin in #a completer:
            #   chaine += str(voisin) + ' '
            chaine += '\n'
        return #a completer
    
    def __repr__(self):
        '''
        renvoie une chaine qui décrit l'objet
        : return
        type str
        '''
        pass
    
class Graphe_non_oriente_mat(Graphe_oriente_mat) :
    '''
    une classe pour un graphe non orienté avec une matrice adjacente
    '''
    def __init__(self, n):
        pass
    
    def ajouter_arc(self, sommet1, sommet2):
        '''
        ajoute un arc non orienté de sommet1 vers sommet2 et un arc de sommet2 vers sommet2
        : param s1, s2 deux entiers entre 0 et n (exclus)
        type int
        : pas de return, EFFET De BORD sur self
        '''
        pass
        
        
    def __repr__(self):
        '''
        renvoie une chaine qui décrit l'objet
        : return
        type str
        '''
        pass

    
###################DOCTEST
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose = True)
