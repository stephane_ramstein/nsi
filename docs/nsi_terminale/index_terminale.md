# Classe de terminale


## Consignes de rentrée

- **matériel obligatoire** : évalué chaque trimestre
    - un classeur avec des pochettes transparentes : pour ranger l'ensemble du cours dans l'ordre. Pour chaque chapitre il faudra utiliser :
        - deux pochettes pour le cours : une pour le cours et une autre pour la prise de notes
        - deux pochettes par TP ou TD : une pour le sujet et une autre pour la prise de notes
    - une clé USB : pour sauvegarder programmes et documents. Ne pas oublier de sauvegarder son contenu chez soi en cas de perte ou de dysfonctionnement.

- **le cours :** il s'articule entre cours au tableau appuyé d'un diaporama, lecture du livre de NSI à la maison, travaux pratiques avec ou sans ordinateur et travaux dirigés sans ordinateur. Par séance, il faut à minima noter :
    - la date du jour
    - les définitions, explications, remarques, ...
    - les réponses aux questions posées
    - ce qu'on a fait pendant la séance
    - ce qu'il y a à faire pour la prochaine séance

- **remarques sur la prise de notes :**
    - elle ne doit pas forcément être propre ou bien rédigée
    - elle doit être structurée
    - elle doit contenir les explications de l'enseignant, les exemples, les réponses aux questions, les corrections, ...
    - elle doit contenir toutes les informations utiles pour l’apprentissage et les révisions

- **le travail à la maison :** il consiste à rédiger au propre le cours, lire le livre de NSI, faire les exercices, devoirs à la maison, avancer les projets et réviser le tout régulièrement.

- **la préparation au baccalauréat :** il faut travailler régulièrement les annales des années précédentes et tous les exercices pratiques de la banque nationale des sujets afin de bien se préparer aux épreuves écrites et pratiques.

- **les évaluations :** elles consistent en des devoirs surveillés écrits et pratiques sanctionnés par une note sur 20. Les devoirs à la maison, les oraux, les projets, la prise de notes, et la **tenue du classeur** sont aussi évalués sous la forme d'un bonus entre 0 et 2 points. La moyenne des bonus du trimestre s'ajoute à la moyenne des devoirs surveillés. Ainsi, un travail personnel et sérieux permet facilement d'augmenter sa moyenne de 1 point même s'il est incomplet ou contient des erreurs. Un travail non rendu est sanctionné par un malus de 0.5 points. Un travail bâclé ou recopié est automatiquement évalué à 0 points.


**A FAIRE dès le premier cours :**

<!-- - Prise en main de la rédaction en markdown : [md](../outils/initiation_markdown.md) -->

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/) **(progression évaluée en cours)**

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/) **(progression évaluée en cours et à la maison, niveaux 1 et 2 validés fin de semaine prochaine + niveau 3 jusqu'au chapitre 7 inclus validés fin de semaine suivante)**


## Langages et programmation - Mise au point des programmes, gestion des bugs

Un langage de programmation est un langage formel. Son utilisation impose une rigueur mathématique pour l'écrire, l'interpréter et obtenir le bon comportement de la machine. Programmer n'est donc pas une tâche facile. Tout au long de l'apprentissage, le novice, tout au long de son travail, l'expert, rencontre erreurs et bugs. L'expérience personnelle et l'application des bonnes pratiques, issues de l'expérience des experts, permettra d'améliorer quotidiennement la qualité des programmes rédigés.

Pendant l'année, les problématiques classiques seront exposées, les situations rencontrées seront expliquées avec propositions de solutions.

- Prise en main de Python et bonnes pratiques : [md](../outils/initiation_python.md)

- Apprendre en s'amusant avec PY-RATES : [lien](https://py-rates.fr/)

- Parcours Castor : [lien](https://concours.castor-informatique.fr/)

- Parcours Algorea : [lien](https://parcours.algorea.org/contents/4703/)

- Entrainement à la programmation France-IOI : [lien](http://www.france-ioi.org/)


## Révisions - Algorithmes

Révision des algorithmes à connaître et évaluation de leur complexité. Permet de reposer les fondamentaux, notamment le parcours de tableau et les tris.

- Diapo cours : [pdf](./01_revisions_algo/algo_revisions_cours_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_cours_diapo.odp)
- Diapo TD : [pdf](./01_revisions_algo/algo_revisions_TD_diapo.pdf) ou [odp](./01_revisions_algo/algo_revisions_TD_diapo.odp)
- Algorithmes à connaitre : [md](../outils/algorithmes/algorithmes_a_connaitre.md)


## Langages et programmation - Modularité

On aborde la modularité tout en révisant la programmation Python avec l'implémentation des algorithmes de base et l'évaluation de leurs performances.

- Diapo cours : [pdf](./02_modularite/Cours.pdf) ou [odp](./02_modularite/Cours.odp)
- Fiche TP : [pdf](./02_modularite/TP_modularite_algo.pdf) ou [odt](./02_modularite/TP_modularite_algo.odt)
- Modèles de fonction, programme et module Python : [zip](../outils/initiation_python/programmes_python_types/programmes_python_types.zip)


## Langages et programmation - Récursivité

Approche en algorithmique et programmation. Introduction aux fractales.

- Diapo cours TD : [pdf](./03_recursivite/cours_recusivite.pdf) ou [odp](./03_recursivite/cours_recusivite.odp)
- Fiche TP : [pdf](./03_recursivite/TP_recursivite.pdf) ou [odt](./03_recursivite/TP_recursivite.odt)
- Fiche arbre d'appel : [pdf](./03_recursivite/arbre_appel.pdf) ou [odp](./03_recursivite/arbre_appel.odp)
- Illustration de la recherche dichotomique récursive : [pdf](./13_techniques_programmation/SR_08_dichotomie_recursive.pdf) ou [odp](./13_techniques_programmation/SR_08_dichotomie_recursive.odp)


## Bases de données - Bases de données relationnelles

- Diapo de cours sur l'introduction aux bases de données : [pdf](./09_bases_de_donnees/diapo_cours.pdf) ou [odp](./09_bases_de_donnees/diapo_cours.odp)
- Fiche sur l'utilisation de SQLite3 : [pdf](./09_bases_de_donnees/fiche_sqlite.pdf) ou [odt](./09_bases_de_donnees/fiche_sqlite.odt)
- Fiche sur le langage SQL : [pdf](./09_bases_de_donnees/fiche_SQL.pdf) ou [odt](./09_bases_de_donnees/fiche_SQL.odt)
- Fiche TP 1 sur la création de tables : [pdf](./09_bases_de_donnees/TP_creations_tables.pdf) ou [odt](./09_bases_de_donnees/TP_creations_tables.odt)
- Fiche TP 2 sur le langage SQL : [pdf](./09_bases_de_donnees/TP_manipulation_tables.pdf) ou [odt](./09_bases_de_donnees/TP_manipulation_tables.odt)
    - Base de données pour le TP 2 : [zip](./09_bases_de_donnees/communes_nord.zip)
- Fiche d'exercices : [pdf](./09_bases_de_donnees/exosrevisionBDR.pdf) ou [odt](./09_bases_de_donnees/exosrevisionBDR.odt)

<!-- - Résumé de cours : [md](./09_bases_de_donnees/resume_cours_1/resume.md) -->


<!-- - TD sur le vocabulaire et les relations entre tables : [pdf](./09_bases_de_donnees/fiche_exercices_bdr.pdf) ou [odt](./09_bases_de_donnees/fiche_exercices_bdr.odt) -->


<!-- - Fiche TP supplémentaire sur la création de tables : [pdf](./09_bases_de_donnees/tp_1_marette.pdf) ou [odt](./09_bases_de_donnees/tp_1_marette.odt)
    - Base de données pour le TP : [zip](./09_bases_de_donnees/lycee.zip) -->


<!--
- Fiche TD 2 sur le langage SQL : [pdf](./09_bases_de_donnees/TD_2.pdf) ou [odt](./09_bases_de_donnees/TD_2.odt)
- Base de donnée pour corriger le TD 2 : [zip](./09_bases_de_donnees/biblio.zip)

- Correction du TP 2 : [pdf](./09_bases_de_donnees/correction2TP.pdf) ou [odt](./09_bases_de_donnees/correction2TP.odt)
- Utilisation d'une base de données avec Python : [zip](./09_bases_de_donnees/biblio_python.zip)

- Fiche TD 3 sur les anomalies dans les bases de données relationnelles : [pdf](./09_bases_de_donnees/anomalies.pdf)
- Correction de l'exercice 6 : [pdf](./09_bases_de_donnees/correction_exercice_6.pdf) ou [odt](./09_bases_de_donnees/correction_exercice_6.odt) -->


## Structures de données - Programmation objet

On approfondit la structuration des données et programmes. Encapsulation.

- Diapo cours : [pdf](./04_programmation_objet/cours_objets.pdf) ou [odp](./04_programmation_objet/cours_objets.odp)
- Fiche TP : [pdf](./04_programmation_objet/TP_programmation_objet.pdf) ou [odt](./04_programmation_objet/TP_programmation_objet.odt)
- Fiche TP "Un bouquet de fleurs" : [pdf](./04_programmation_objet/exo_fleurs.pdf) ou [odt](./04_programmation_objet/exo_fleurs.odt)
- Modèle de classe : [zip](../outils/initiation_python/programmes_python_types/modele_classe_objet.zip)
- Modèle de classe : [zip](../outils/initiation_python/programmes_python_types/modele_classe_objet.zip)


## Structures de données - Type abstrait LISTE

Le type abstrait LISTE et ses implémentations sont abordées à travers structures de données tableaux statiques et listes chainées. Les types abstraits et les structures de données qui les implémentes sont spécifiés par leur interface.

- Diapo de cours : [pdf](./05_listes_chainees_piles_files/cours.pdf) ou [odp](./05_listes_chainees_piles_files/cours.odp)
- TP d'introduction : [pdf](./05_listes_chainees_piles_files/TP_introduction_liste_chainees.pdf) ou [odt](./05_listes_chainees_piles_files/TP_introduction_liste_chainees.odt)
- TP interface d'une liste chaînée : [pdf](./05_listes_chainees_piles_files/TP_interface_liste_chainees.pdf) ou [odt](./05_listes_chainees_piles_files/TP_interface_liste_chainees.odt)


## Architectures matérielles, OS et réseaux - Circuits intégrés

Depuis l'invention du circuit intégré par Texas Instrument, le microprocesseur n'a cessé d'évoluer vers plus de puissance grâce à la miniaturisation de ses composants, tout en limitant la consommation d'énergie.

- Diapo cours : [pdf](./06_circuits_integres/cours.pdf) ou [odp](./06_circuits_integres/cours.odp)
- Fabrication d'un microprocesseur en images : [video](https://www.youtube.com/watch?v=d9SWNLZvA8g) et [images et texte](https://www.tomshardware.fr/diapo-la-fabrication-dun-processeur-expliquee-en-images/)


## Structures de données - Type abstrait PILES et FILES

Il existe de nombreuses structures de données différentes. Choisir la plus adaptée permettra de traiter les données de manières plus simple, efficace et rapide.

On retrouve dans les piles une partie des propriétés vues sur les  listes. Dans les piles, il est uniquement possible de manipuler le  dernier élément introduit dans la pile. On prend souvent l'analogie avec une pile d'assiettes : dans une pile d'assiettes la seule assiette directement accessible et la dernière assiette qui a été déposée sur  la pile. 	

Comme les piles, les files ont des points communs avec les listes.  Différences majeures : dans une file on ajoute des éléments à une  extrémité de la file et on supprime des éléments à l'autre extrémité. On prend souvent l'analogie de la file d'attente devant un magasin  pour décrire une file de données. 	

<!-- - Diapo cours : [pdf](./05_listes_chainees_piles_files/diapo_piles_files.pdf) ou [odp](./05_listes_chainees_piles_files/diapo_piles_files.odp) -->
<!-- - Résumé de cours : [md](./05_listes_chainees_piles_files/cours_piles_files.md) -->
- Fiche exercices sur les piles et les files : [pdf](./05_listes_chainees_piles_files/Exercices_piles_files.pdf) ou [md](./05_listes_chainees_piles_files/Exercices_piles_files.md)
- Fiche TP sur les piles et les files : [pdf](./05_listes_chainees_piles_files/TP_piles_files.pdf) ou [odt](./05_listes_chainees_piles_files/TP_piles_files.odt)
    - Module python pour le TP : [py](./05_listes_chainees_piles_files/module_liste_chainee.py)

<!-- - Fiche TP sur les piles : [pdf](./05_listes_chainees_piles_files/Implémentation_des_piles.pdf) ou [md](./05_listes_chainees_piles_files/Implémentation_des_piles.md) -->
<!-- - Fiche TP sur les files : [pdf](./05_listes_chainees_piles_files/Implémentation_des_files.pdf) ou [md](./05_listes_chainees_piles_files/Implémentation_des_files.md) -->
<!-- - Fichiers Python pour les TP : [zip](./05_listes_chainees_piles_files/fichiers_python.zip) -->


## Architectures matérielles, OS et réseaux - Gestion des processus et des ressources

- Résumé de cours : [md](./07_processus_ressources/cours.md)
- Diapo cours : [pdf](./07_processus_ressources/cours.pdf) ou [odp](./07_processus_ressources/cours.odp)
- Fiche TP sur la gestion des processus : [pdf](./07_processus_ressources/TP_processus.pdf) ou [odt](./07_processus_ressources/TP_processus.odt)
- Fiche TP sur la gestion des ressources : [pdf](./07_processus_ressources/TP_fork_interblocage.pdf) ou [odt](./07_processus_ressources/TP_fork_interblocage.odt)


## Structures de données et algorithmiques - Arbres binaires, arbres binaires de recherche, autres structures arborescentes

- Diapo cours : [pdf](./08_arbres/Diapo_SR.pdf) ou [odp](./08_arbres/Diapo_SR.odp)
- Fiche exos 1 : [md](./08_arbres/exos_SR.md) ou [pdf](./08_arbres/exos_SR.pdf)
- Fiche tp 1 sur la construction d'arbre binaires : [pdf](./08_arbres/tp_arbres_binaires.pdf) ou [odt](./08_arbres/tp_arbres_binaires.odt)
- Fiche tp-cours sur les fonctions usuelles des arbres binaires : [pdf](./08_arbres/tp_fonctions_arbres.pdf) ou [odt](./08_arbres/tp_fonctions_arbres.odt)
- Fiche tp-cours sur les arbres binaires de recherche : [pdf](./08_arbres/tp_arbres_binaires_recherche.pdf) ou [odt](./08_arbres/tp_arbres_binaires_recherche.odt)

<!-- - Résumé de cours : [md](./08_arbres/cours/arbres_binaires.md) -->
<!-- - Fiche TP 1 sur le parcours en profondeur : [md](./08_arbres/tp/partie1/parcours_profondeur.md)
- Fiche TP 2 sur le parcours en largeur : [md](./08_arbres/tp/partie2/parcours_largeur.md)
- Fiche TP 3 sur les arbres binaires de recherche : [md](./08_arbres/tp/partie3/ABR.md)
- Module à utiliser pour le TP : [zip](./08_arbres/tp/arbres_binary_tree.zip)
- Fiche exercices 1 : [pdf](./08_arbres/exos/exos.pdf) ou [md](./08_arbres/exos/exos.md)
- Fiche exercices 2 : [pdf](./08_arbres/exos/exo2.pdf) ou [md](./08_arbres/exos/exo2.md) -->


## Architectures matérielles, OS et réseaux - Protocoles de routage

Internet est l'assemblage de réseaux autonomes. Après leur présentation et un rappel des notions élémentaires, le cours étudie les deux protocoles permettant d'inscrire dans les tables de routage les chemins que doivent emprunter les paquets de données dans ce type de réseau : **R**outing **I**nformation **P**rotocol et **O**pen **S**hortest **P**ath **F**irst.

- Diapo de cours : [pdf](./12_protocoles_reseaux/cours.pdf) ou [odp](./12_protocoles_reseaux/cours.odp)
    - Algorithme de Dijkstra, application sur le graphe : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_graphe.odp)
    - Algorithme de Dijkstra, application en tableau : [pdf](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.pdf) ou [odp](./12_protocoles_reseaux/algorithme_Dijkstra_tableau.odp)

- Fiche TD sur les protocole RIP et OSPF : [pdf](./12_protocoles_reseaux/TD_protocoles_reseaux.pdf) ou [odt](./12_protocoles_reseaux/TD_protocoles_reseaux.odt)
    - Tables de routage vierges pour le TD : [ods](./12_protocoles_reseaux/Tables_routage_vierges.ods)
    - Quelques topologies : [pdf](./12_protocoles_reseaux/topo_reseaux.pdf) ou [odp](./12_protocoles_reseaux/topo_reseaux.odp)

- TP illustrant le protocole RIP en Python : [pdf](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.pdf) ou [odt](./12_protocoles_reseaux/Implémentation_du_protocole_RIP_en_python.odt)

- Révisions de première : [pdf](../nsi_premiere/07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.pdf) ou [odp](../nsi_premiere/07_architecture_reseau/01_cours_reseau/SR_03_Architecture_reseau.odp)
    * Vidéo sur l'histoire d'internet : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/0_Historique_et_Principe.mp4)
    * Vidéo sur le fonctionnement d'internet : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/1_Internet_Comment_ca_marche.mp4)
    * Vidéo sur le protocole TCP-IP : [mp4](../nsi_premiere/07_architecture_reseau/01_cours_reseau/3_MOOC_SNT_Internet_Protocole_TCP_IP.mp4)
    * Fiche sur les protocoles IP et TCP : [pdf](../nsi_premiere/07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.pdf) ou [odt](../nsi_premiere/07_architecture_reseau/01_cours_reseau/04d_Activite_SNT_Fiche_eleve_TCP_IP_Ports.odt)


## Algorithmique - Diviser pour régner et Programmation dynamique

La méthode diviser pour régner est une méthode algorithmique qui consiste à décomposer un problème complexe en problèmes simples, résoudre les problèmes simples et combiner les solutions obtenues pour obtenir la solution du problème initial. La méthode fournit en général des algorithmes de complexité faible ce qui en fait son intérêt. L'algorithme d'Euclide, 300 av JC, pour calculer le plus grand commun diviseur de deux nombres peut être vu comme un algorithme diviser pour régner. Son introduction en informatique se fait via le tri radix en 1929, décrit pour les machines IBM à cartes à trou.

- Diapo de cours : [pdf](./13_techniques_programmation/cours.pdf) ou [odp](./13_techniques_programmation/cours.odp)
- Fiche TD sur les techniques de programmation : [pdf](./13_techniques_programmation/tp_techniques_programmation.pdf) ou [odt](./13_techniques_programmation/tp_techniques_programmation.odt)
    - Fichiers images pour le TP : [manchot](./13_techniques_programmation/manchot.png) et [lapins](./13_techniques_programmation/lapins.png)
- Illustration de la recherche dichotomique récursive : [pdf](./13_techniques_programmation/SR_08_dichotomie_recursive.pdf) ou [odp](./13_techniques_programmation/SR_08_dichotomie_recursive.odp)
- Illustration de la recherche dichotomique non récursive : [pdf](../nsi_premiere/10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.pdf) ou [odp](../nsi_premiere/10_algorithmique/recherche_dichotomique/SR_07_dichotomie_non_recursive.odp)
- Illustration de la fusion récursive : [pdf](./13_techniques_programmation/SR_05_fusion_recursive.pdf) ou [odp](./13_techniques_programmation/SR_05_fusion_recursive.odp)
- Illustration de la fusion non récursive : [pdf](./13_techniques_programmation/SR_04_fusion_non_recursive.pdf) ou [odp](./13_techniques_programmation/SR_04_fusion_non_recursive.odp)
- Illustration du tri fusion : [pdf](./13_techniques_programmation/SR_06_tri_fusion.pdf) ou [odp](./13_techniques_programmation/SR_06_tri_fusion.odp)

<!-- ## Algorithmique - Diviser pour régner

- Cours : [pdf](./13_techniques_programmation/cours_Diviser_regner.pdf) ou [odt](./13_techniques_programmation/cours_Diviser_regner.odt)
- Fiche TP : [pdf](./13_techniques_programmation/TP_Diviser_regner.pdf) ou [odt](./13_techniques_programmation/TP_Diviser_regner.odt) -->


## Architectures matérielles, OS et réseaux - Sécurisation des communications

Depuis la seconde guerre mondiale et l'avènement de l'informatique moderne, la cryptologie est passée du statut d'art utilisé essentiellement par les militaires au statut de science utilisée par tous. Elle regroupe la cryptographie, élaborant et caractérisant les systèmes de chiffrage/déchiffrage des données et la cryptanalyse, tentant de casser les systèmes avec un nombre d'information limité.

- Diapo de cours, généralités et cryptage symétrique : [pdf](./18_cryptographie/cours_cryptographie.pdf) ou [odp](./18_cryptographie/cours_cryptographie.odp)
- Fiche TP, cryptage symétrique : [pdf](./18_cryptographie/TPcrypto.pdf) ou [odt](./18_cryptographie/TPcrypto.odt)
- Fiche cours-TD, cryptage asymétrique : [pdf](./18_cryptographie/SR_08_RSA.pdf) ou [odt](./18_cryptographie/SR_08_RSA.odt)

<!-- - Cours à compléter : [md](./18_cryptographie/cours_marette/Sécurisation_des_communication_a_trou.md)
- TP sur le cryptage symétrique : [md](./18_cryptographie/cours_marette/Les_chiffrements_symétriques.md)
    - Code python à compléter : [py](./18_cryptographie/cours_marette/codage_symetrique.py) -->


## Structures de données et algorithmique - Graphes et parcours en profondeur et en largeur

- Diapos de cours : [pdf](./14_graphes/cours_graphes.pdf) ou [odp](./14_graphes/cours_graphes.odp)
- Cours : [pdf](./14_graphes/Graphes_Vanoverberghe/Graphes_Cours.pdf) ou [md](./14_graphes/Graphes_Vanoverberghe/Graphes_Cours.md)
- Fiche d'exercices : [pdf](./14_graphes/Graphes_Vanoverberghe/Exercices.pdf) ou [md](./14_graphes/Graphes_Vanoverberghe/Exercices.md)
- Travail pratique sur la prise en main de NetworkX : [pdf](./14_graphes/Graphes_Vanoverberghe/TP_1.pdf) ou [md](./14_graphes/Graphes_Vanoverberghe/TP_1.md)

<!-- 
- Cours : [pdf](./14_graphes/cours_graphes.pdf) ou [odp](./14_graphes/cours_graphes.odp)
- Travail pratique construction de graphes : implémenter les classes **Graphe_oriente** et **Graphe_non_oriente** à partir de matrices et de listes d'adjacence
- Travail dirigé : [pdf](./14_graphes/exercices.pdf)
- Travail pratique parcours de graphes : [pdf](./14_graphes/exercices_parcours.pdf)
- Travail pratique détection de chemin et de cycles : [pdf](./14_graphes/exercice_application_parcours-2.pdf)
- Travail pratique "En voyage !" : [pdf](./14_graphes/exoAmerique.pdf) ou [odt](./14_graphes/exoAmerique.odt) -->

<!-- - Travail dirigé : [pdf](./14_graphes/TD_Graphe.pdf)
- Travail pratique sur l'implémentation des graphes : [pdf](./14_graphes/TP_Graphe.pdf)
    - Corrigé : [py](./14_graphes/Correction_module_graphe_1.py)
- Travail pratique sur le parcours de graphes : [pdf](./14_graphes/TP_Graphe_Parcours.pdf) -->

<!-- - Fiche TD-cours : [pdf](./14_graphes/Cours_TD_Graphe.pdf) ou [odt](./14_graphes/Cours_TD_Graphe.odt)
- Fiche TP sur l'implémentation des graphes avec des listes : [md](./14_graphes/implementation_liste.md)
    - fichier python pour le TP : [py](./14_graphes/module_graphe_liste.py)
- Fiche TP l'implémentation des graphes avec des matrices : [md](./14_graphes/implementation_mat.md)
    - fichier python pour le TP : [py](./14_graphes/module_graphe_mat.py)
- Fiche TD algorithmes sur les graphes : [pdf](./14_graphes/Cours-TD_AlgoGraphe.pdf) ou [odt](./14_graphes/Cours-TD_AlgoGraphe.odt) -->

<!-- - Résumé de cours : [pdf](./14_graphes/cours/graph.pdf) ou [md](./14_graphes/cours/graph.md)
- Diapo de cours : [pdf](./14_graphes/diapo.pdf) ou [odp](./14_graphes/diapo.odp)
- Fiche TD d'applications directes du cours : [pdf](./14_graphes/td/td.pdf) ou [md](./14_graphes/td/td.md)
- Fiche TP présentant une classe pour travailler sur les graphes : [pdf](./14_graphes/tp/tp.pdf) ou [md](./14_graphes/tp/tp.md)
- Fiche TD sur une mise en situation : [pdf](./14_graphes/td2/td.pdf) ou [md](./14_graphes/td2/td.md)
- Fiche TP sur la multiplication de matrices : [pdf](./14_graphes/tp2/corriger.pdf) ou [md](./14_graphes/tp2/corriger.md) -->


# Révisions - Algorithmes gloutons et des plus proches voisins

- TP-cours : [md](./13_techniques_programmation/revisions_glouton/TP_algo_04_algorithmes_intelligents.md)

<!-- - Algorithme glouton - Résumé de cours : [md](./13_techniques_programmation/revisions_glouton/cours.md)
- Algorithme glouton - Diapo de cours : [pdf](./13_techniques_programmation/revisions_glouton/diapo.pdf) ou [odp](./13_techniques_programmation/revisions_glouton/diapo.odp)
- Algorithme glouton - Fiche TP : [pdf](./13_techniques_programmation/revisions_glouton/TP_Le_probleme_des_pieces_de_monnaie.pdf) ou [md](./13_techniques_programmation/revisions_glouton/TP_Le_probleme_des_pieces_de_monnaie.md)
    - Fichier Python pour le TP : [zip](./13_techniques_programmation/revisions_glouton/monnaie_eleve.zip)
<br>

- Algorithme knn - Résumé de cours : [md](./13_techniques_programmation/revisions_knn/cours.md)
- Algorithme knn - Diapo de cours : [pdf](./13_techniques_programmation/revisions_knn/diapo.pdf) ou [odp](./13_techniques_programmation/revisions_knn/diapo.odp)
- Algorithme knn - Fiche TP : [pdf](./13_techniques_programmation/revisions_knn/TP_KNN.pdf) ou [md](./13_techniques_programmation/revisions_knn/TP_KNN.md)
    - Fichier Python pour le TP : [zip](./13_techniques_programmation/revisions_knn/vetements_eleve.zip) -->


## Projets

- Animation avec tkinter : [pdf](../outils/interfaces_graphiques/tkinter_animation.pdf) ou [odt](../outils/interfaces_graphiques/tkinter_animation.odt)
- Un labyrinthe : [md](../projets/labyrinthe/laby.md)


## Corrections

[Dépôt des corrections](https://gitlab.com/stephane_ramstein/corrections/-/tree/main/TNSI)

