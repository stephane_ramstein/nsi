## Le problème des pièces de monnaie

### Ennoncé

On dispose de pièces de monnaie dont les montants sont des nombres entiers de l’ unité de monnaie : $p0,p1,p2,...,pn$.

Pour payer une certain somme $S$ (entier naturel), on aimerait utiliser le nombre minimal de pièces possibles.

*Exemple : l'euro*

Dans le système monétaire européen on utilise les montants suivants :

$p1=50€, p2=20€, p3=10€, p4=5€, p6=2€ et p7=1€$



Comment payer la somme $S$=528€ en utilisant le moins de pièces possibles ?

```plaintext

```



N'y-a-t-il qu'une seule façon de payer cette somme ?

```plaintext

```



### Implémentation

- Proposer un algorithme, exprimé en langage naturel, permettant de résoudre ce problème .

  ```plaintext
  
  
  
  
  
  
  
  
  
  
  
  ```

  

*Remarque :*

Il s’agit effectivement d’un algorithme glouton, la plus grande valeur de pièce étant systématiquement choisie si sa valeur est inférieure à la somme à rendre. Ce choix ne garantit en rien l’optimalité globale de la solution. Le choix fait est considéré comme pertinent et permet d’avancer plus avant dans le calcul.

Nous allons maintenant implémenter l'algorithme en Python :

- télécharger le fichier monnaie_eleve.py sur le git
- On utilisera une liste nommée `SYSTEME` définie globalement et constante ,  triée dans le sens décroissant pour les montants utilisés.

- nous aurons besoin de deux fonctions de paramètre `somme_a_rendre`:

  - la première **plus_grosse_piece** qui renvoie la plus grosse pièce du système inférieur ou égale à la somme à rendre

  - l'autre **rendu_monnaie** qui renvoie une solution gloutonne sous forme d'un dictionnaire dont les clés sont les sommes des pièces/billetes utilisés et les valeurs le nombre de fois qu'on les utilise pour rendre la monnaie.Voici des exemples d'exécution de cette fonction:

    ![rendu.png](rendu.png)

    

- Compléter le corps de ces fonctions. 

- tester votre fonction sur 528.Obtenez vous le meme résultat que précedemment?

  ```plaintext
  
  ```

  

### Optimal ?

On se pose la question de savoir si cet algorithme est forcément optimal, c'est à dire utilise toujours aussi peu de pièces que possible.

*Exemple : ancien système monétaire britannique*

Ce système utilisait les montants suivants :$ p0=600, p1=30, p2=24, p3=12 , p4=6, p5=2 et p6=1$

- Avec ces montants, peut-on  payer n'importe quelle somme entière ?

  ```plaintext
  
  ```

  

Comment payer  $ S$=48 avec le moins de pièces possibles ?

```plaintext

```



Adaptez `monnaie_eleve.py` afin que cette fonction détermine une façon de payer. Quel est le résultat fourni ? Est-ce optimal ?

```plaintext

```



*Remarques :*

Pour éviter ce phénomène, les pays émetteurs des jeux de monnaie utilisent toujours des jeux tels que le résultat de l'algorithme glouton soit toujours optimal. On dit que ces systèmes sont **canoniques**.



