# **Structure de données : les arbres binaires**

## **Exercices d'applications**

![arbres_1](./arbres_1.png)

**Question 1** - Remplir le tableau suivant avec les arbres ci dessus :

|               |arbre 1|arbre 2|arbre 3|
|:-------------:|:-----:|:-----:|:-----:|
|taille         |       |       |       |
|profoneur de 1 |       |       |       |
|profoneur de 2 |       |       |       |
|profoneur de 5 |       |       |       |
|profoneur de 7 |       |       |       |
|profoneur de 10|       |       |       |
|profoneur de 13|       |       |       |
|profoneur de 14|       |       |       |
|hauteur        |       |       |       |

---

**Question 2** - Donner les résultats des parcours en profondeur :

- **préfixe** pour le premier arbre
- **infixe** pour le second arbre
- **postfixe** pour le troisième arbre

---







**Question 3** - En utilisant le module ```arbres_binary_tree```, sur l'arbre ci dessous :

- donner la définition en **plusieurs affectations**, donc **plusieurs lignes**, de l'arbre ayant pour racine le noeud **4**.
- donner la définition en **une seule affectation**, donc **une seule ligne**, de l'arbre ayant pour racine le noeud **1**.

![arbres_2](./arbres_2.png)

---

**Question 4** - Dessiner les arbres de ces définitions :

```python
vide = BinaryTree()

# Premier arbre : F
A = BinaryTree("A", vide, vide)
E = BinaryTree("E", vide, vide)
I = BinaryTree("I", vide, vide)

B = BinaryTree("B", A, vide)
D = BinaryTree("D", vide, E)
H = BinaryTree("H", vide, I)

C = BinaryTree("C", B, D)
G = BinaryTree("G", vide, H)

F = BinaryTree("F", C, G)

# Deuxième arbre : J
J = BinaryTree("A", BinaryTree("B", vide, BinaryTree("F", vide, vide)), BinaryTree("D", BinaryTree("E", vide, vide), BinaryTree("C", vide, vide)))
```















**Question 5** - Parmi les 5 arbres suivants, lesquels sont **Strict**, **Complet**, **parfait** et **équilibré** ?

![arbres_3](./arbres_3.png)
