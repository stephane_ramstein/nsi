# Système d'exploitation LINUX et processus

Lorsque la machine est en fonctionnement, le processeur exécute des milliers de tâches par seconde sans que l'utilisateur ne s'en aperçoive. Dans ce chapitre, il s'agit d'approfondir le cours de première sur les systèmes d'exploitation, plus particulièrement le fonctionnement de l'ordonnanceur et la gestion des processus.

## Démarrage d'une machine - exemple du Raspberry PI 3

A la mise sous tension de la machine le système d'exploitation n'est pas immédiatement en fonctionnement. Il n'est même pas en exécution. Avant qu'il ne soit en mesure de mettre à disposition l'ensemble des ressources matérielles aux applications et aux utilisateurs, la machine doit démarrer, se configurer et lancer le système d'exploitation. Ce n'est qu'à ce moment qu'il va porter la machine dans un état utilisable.

Les machines à base de microprocesseurs (CPU et SOC) fonctionnent sur ce principe. Dans le cas des microcontrôleurs, il n'y a pas de système d'exploitation. Ces machines spécialisées sont conçues pour n'exécuter qu'un seul programme.

Le Rasberry PI 3 (2018) est un SOC BCM2837B0 de chez Broadcom composé d'un CPU 64 bits à quatre cœurs ARM Cortex-A53, cadencé à 1,4 GHz et d'un GPU VideoCore IV. Il possède une mémoire vive SDRAM LPDDR2 de 1 Go et utilise une carte microSD comme support de masse. Son système d'exploitation privilégié est LINUX, bien que d'autres soient disponibles.

![](assets/cours-7ca8fa63.png)

### Procédure de boot

Ce sont les différentes étapes entre la mise sous tension et le lancement du système d'exploitation.

1. Seul un coeur du GPU démarre et exécute directement le programme de démarrage (bootloader) stocké en binaire dans le mémoire morte ROM. Avec ce programme le GPU met en service la carte microSD et copie le programme binaire bootcode.ini de la carte microSD vers la mémoire cache.

2. Le coeur du GPU exécute le programme présent dans la mémoire cache. Il permet de configurer le GPU, de le démarrer entièrement, de mettre en service la SDRAM et de copier le programme bianire start.elf de la carte microSD vers la partie haute de la SDRAM.

3. Le GPU exécute le programme présent dans la partie haute de la SDRAM. Il permet de partitionner la SDRAM en deux, la partie haute est réservée au GPU et la partie basse au CPU. Il contient aussi toutes les fonctions permettant d'exploiter toutes les fonctionnalités du GPU. Le GPU copie le programme binaire kernel.img, qui est le noyau du système d'exploitation, dans la partie basse de la SDRAM. Le GPU réveille le CPU et se met en veille.

4. Le CPU démarre le noyau du système d'exploitation, notamment l'ordonnanceur qui ne s'arrêtera plus de fonctionner. Le noyau recherche le système de fichier LINUX présent sur la carte microSD et le rend accessible. On dit qu'il le monte. Le système de fichier contient l'ensemble des services du système d'exploitation et les différentes applications installées sur la machine. Le dossier /lib contient notamment le programme systemd qui permet de gérer l'ensemble du système d'exploitation. Le noyau le charge et l'exécute. A partir de cet instant, le système d'exploitation prend entièrement la main sur la machine.


### Démarrage du système d'exploitation

systemd va se charger du démarrage du système d'exploitation en le portant dans un état utilisable par les applications et l'être humain. Pour cela il lance en parallèle plusieurs services essentiels l'utilisation du système en mode texte (basic.target) puis en mode graphique (graphical.target).

![](assets/cours-f7fafed1.png)

systemd est aussi le premier processus lancé et sera le dernier à s'arrêter. Il va gérer le système d'exploitation et les applications à travers notamment sa gestion des processus. Sa gestion des ressources matériel le sera à travers son interaction avec le noyau.

![](assets/cours-56c799f7.png)


## Gestion des processus

A part systemd, un processus ne peut être lancé que par un autre processus. On parle de processus parent et de processus enfant. L'exécution des processus est donc hiérarchisée. Un processus parent peut avoir plusieurs processus enfants mais un processus enfant ne peut avoir qu'un seul processus parent. Les processus sont identifiés par leur Processus IDentifier (PID). Ainsi systemd est le parent de tous les processus. Son PID est de 1.

Les processus sont mis en exécution par le processeur chacun leur tour grâce à un programme du noyau du système d'exploitation appelé ordonnanceur.

### Statut d'un processus

Après sa création un processus peut être dans plusieurs états. Les états d'un processus s'exécutant normalement sont :

* R : running. Le processus est en train d'être exécuté par le processeur ou est prêt à être exécuté.
* S : sleeping. Le processus est en veille. Il attend un évènement pour reprendre son exécution.
* I : sleeping > 20 s. Le processus est en veille depuis plus de 20 s.
* X : terminated. Le processus est terminé et les ressources qu'il utilisait sont en cours de libération. Cet état est transitoire et ne devrait pas être observable.

D'autres états révélant un fonctionnement anormal du processus peuvent être observés:

* D : blocked. Etat bloqué. Le processus a déclenché une tâche non interruptible et est en attente.
* T : stopped. Le processus a été mis en pause par une application tierce ou l'utilisateur. Il doit attendre l'autorisation de reprendre son exécution.
* Z : zombie. Le processus est terminé mais ses ressources n'ont pas été libérées. Il est notamment toujours présent en mémoire.

### Cycle de vie d'un processus

Le cycle de vie d'un processus est représenté sur le diagramme ci-dessous. Il est orchestré par l'ordonnanceur qui organise l'exécution des processus par le processeur.

![](assets/cours-87c50c43.png)

1. Après sa création le processus entre dans la file d'attente de l'ordonnanceur. Il est prêt à être exécuté.
2. Le processus est élu par l'ordonnanceur pour être exécuté par le processeur.
3. Le processus a épuisé son temps d'exécution et retourne dans la file d'attente de l'ordonnanceur.
4. Le processus a besoin doit attendre une information pour poursuivre son exécution. Il est mis en attente.
5. L'information qu'attendait le processus est disponible. Le processus retourne dans la file d'attente de l'ordonnaceur.
6. Lors de son exécution par le processeur, le processus est arrivé au terme de son programme.

### Fonctionnement de l'ordonnanceur

Dans les systèmes multitâche, il y a toujours plus de processus à exécuter en même temps qu'il n'y a de coeurs CPU. Ainsi un algorithme doit orchestrer l'exécution des processus en décidant quel processus doit être exécuté par quel coeur CPU. L'ordonnanceur est composé de cet algorithme et d'une file d'attente dans laquelle son enfilés les processus en attente d'être exécuté.

L'ordre d'exécution des processus et leur temps d'exécution alloué est décidé par l'algorithme. Plusieurs méthodes existent comme :

* Round-robin (ou méthode du tourniquet)
* Earliest Deadline First scheduling
* Shortest Job First ou Shortest Job Next
* Completely Fair Scheduler

Il n'existe pour le moment pas d'algorithme idéal. Ils peuvent être de deux types :

* en temps partagé : partage le temps d'exécution entre les processus en se basant sur des règles de priorité.
* en temps réel : optimise le temps d'exécution des processus afin que les processus devant se terminer à un instant fixé le soit effectivement. Ces algorithmes sont aussi capables de mettre en pause les processus afin d'exécuter un processus particulier lorsqu'un évènement extérieur survient.

Une fois leur ordre et leur temps d'exécution déterminé, les processus sont enfilés dans la file d'attente de l'ordonnanceur. Les processus sont exécutés les uns après les autres par le processeur.

![](assets/cours-cc3c9e16.png)

Lorsque le processus passe en exécution on dit qu'il est élu par l'ordonnanceur. Il s'exécute alors pendant le temps qui lui est alloué sauf si un évènement particulier survient (besoin d'une ressource non disponible, évènement extérieur, ...). Son état est alors sauvegardé dans une zone mémoire appelée Process Control Block (PCB), notamment son PID et celui de son parent, le numéro de la prochaine ligne de code à exécuter et les données qu'il utilise.

![](assets/cours-5d7d1ab6.png)

A ce moment, l'ordonnanceur passe la main au processus suivant en chargeant son état à partir de son PCB. Cette opération s'appelle la commutation de contexte.

![](assets/cours-c31baa91.png)


## Concurrence entre processus

### Concurrence compétitive et concurrence coopérative

### L'interblocage
