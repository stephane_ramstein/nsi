# Eléments de correction 2

## Exercice 1

Le but de cet exercice est de calculer la moyenne des éléments d'une liste la première façon est itérative(en parcourant les éléments de la liste), la seconde est en utilisant les fonctions python

```python
def moyenne(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=0
        for elem in tab:
            somme+=elem
        moyenne=somme/len(tab)
        return moyenne
```

ligne 1: si la liste est vide

ligne 2: renvoie de la chaine de caractère erreur

ligne 3: sinon donc si la liste n'est pas vide

ligne 4: initialisation de la variable somme à 0, cette variable contiendra la somme des éléments de la liste

ligne 5: parcours des éléments de la liste

ligne 6: ajout de l'élément à somme

ligne 7: stockage dans moyenne la moyenne

ligne 8: renvoie de la moyenne

```python
def moyenne_python(tab):
    if len(tab)==0:
        return "erreur"
    else:
        somme=sum(tab)
        moyenne=somme/len(tab)
        return moyenne
```

Dans cette version nous allons utiliser la fonction sum du module list.

ligne 1: si la liste est vide

ligne 2: renvoie de la chaine de caractère erreur

ligne 3: sinon donc si la liste n'est pas vide

ligne 4: stockage dans somme la somme des éléments du tableau

ligne 5: stockage dans moyenne la moyenne

ligne 6: renvoie de la moyenne

## Exercice 2

Le but de cet exercice est de faire un tri tout en partionnant une liste en 3 parties. Il fait travailler les indices dans une liste

```python
def tri(tab):
    #i est le premier indice de la zone non triee, j le dernier indice. 
    #Au debut, la zone non triee est le tableau entier.
    i= 0
    j= len(tab)-1
    while i != j :
        if tab[i]== 0:
            i= i+1
        else :
            valeur = tab[j]
            tab[j] = 1
            tab[i]=valeur
            j= j-1
    return tab
```

ligne 1: initialisation de i à 0

ligne 2: initialisation de j au dernier indice du tableau

ligne 3: tant que i est différent de j

ligne 4: si l'élément d'indice i est égale à 0, ici l'élément est donc à la bonne place

ligne 5: incrémentation  de 1 de i

ligne 6: sinon, on est donc dans le cas ou l'élément d'indice i est égale à 1

ligne 7: on stocke  la valeur de tab[j] dans valeur afin de la gardé en mémoire, tab[j] est le dernier élément non triée

ligne 8:  on affecte 1 à j , 1 est la valeur de tab[i]

ligne 9: on affecte à tab[i] l'ancienne valeur de tab de j

ligne 10: on décrement j de 1, la liste trié de 1 est plus grande

ligne 11: on renvoie le tableau trié

 