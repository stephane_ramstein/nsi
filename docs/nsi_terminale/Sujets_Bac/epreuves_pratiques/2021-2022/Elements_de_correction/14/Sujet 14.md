# Sujet 14

## Exercice 1

Le but de cet exercice est de parcourir une chaine de caractère par ses indices et de réaliser une boucle tant que.

```python
def correspond(mot,mot_a_trous):
    res=True
    i=0
    while res and i<len(mot):
        new=mot_a_trous[i]
        if mot[i]!=new and new!='*':
            res= False
        i+=1
    return res
    
```

ligne 1: initialisation du résultat à vrai

ligne 2: initialisation de i , l'indice de parcours à 0

ligne 3: tant que le resultat est à vrai et que i est inférieur à la longueur du mot, ici on va parcourir la chaine 

ligne 4: new prend la valeur de l'élément d'indice i de mot _a_trous

ligne 5 : si new n'est pas égale à l'élément d'indice i de la chaine de base et qu'il est différent de *

ligne 6: initialisation de res à faux,on est dans le cas ou le mot_a_trou ne peut pas représenter la mot 

ligne 7: incrémentation de i

ligne 8: renvoie de res



## Exercice 2

Le but de cet exercice est de parcourir un dictionnaire est de comparer ses  valeurs.

```python
def est_cyclique(plan):
    '''
    Prend en paramètre un dictionnaire `plan` correspondant 
    à un plan d'envoi de messages entre `N` personnes A, B, C, 
    D, E, F ...(avec N <= 26).
    Renvoie True si le plan d'envoi de messages est cyclique
    et False sinon. 
    '''
    personne = 'A'
    N = len(plan)                          
    for i in range(N):
        if plan[personne] == 'A':
            return False
        else:
            personne = plan[personne]
    return True
```

ligne 1: initialisation de personne à 'A', ici personne sera la personne que nous somme en train d'étudier 

ligne 2: initialisation de N à la longueur du plan d'envoi

ligne 3: parcours des N-1 successeurs de 'A'

ligne 4: si le successeur est 'A'

ligne 5: renvoi faux, on est ici dans le cas d'un plan accyclique, A est un des N-1 successeurs

ligne 6: sinon 

ligne 7: personne deviens le successeur de la personne en cours

ligne 8: renvoi de vrai, on est ici dans le cas ou A est le Nème successeur.