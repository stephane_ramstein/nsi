# sujet 23

## exercice 1

Le but de cet exercice est le parcours des clés et valeurs d'un dictionnaire.

```python
def max_dico(dico):
    uti=''
    max=-1
    for cle,valeur in dico.items():
        if valeur>max:
            uti=cle
            max=valeur
    return (uti,max)
```

ligne 1: initialisation de la variable uti à une chaine de caractère vie, cette variable sera le nom de l'utilisateur qui a le plus de like

ligne 2: initialisation  de la variable max à -1, max est le nombre de like maximum on l'initialise à -1 car un abonné à au minimum 0 like ainsi le dictionnaire comportera forcement une valeur supérieur

ligne 3 : parcours du dictionnaire 

ligne 4: test si la valeur du dictionnaire est supérieur au max

ligne 5 : affectation de uti à la cle du dictionnaire 

ligne 6: affection de max à la valeur du dictonnaire 

ligne 7: renvoi du tuple 

## exercice 2

Le but de cet exercice est d'utiliser un algorithme sur une pile.

 ``` python
class Pile:
    """Classe définissant une structure de pile."""
    def __init__(self):
        self.contenu = []

    def est_vide(self):
        """Renvoie le booléen True si la pile est vide, False sinon."""
        return self.contenu == []

    def empiler(self, v):
        """Place l'élément v au sommet de la pile"""
        self.contenu.append(v)

    def depiler(self):
        """
        Retire et renvoie l’élément placé au sommet de la pile,
        si la pile n’est pas vide.
        """
        if not self.est_vide():
            return self.contenu.pop()


def eval_expression(tab):
    p = Pile()
    for element in tab:
        if element != '+' or element != '*':
            p.empiler(element)
        else:
            if element == '+' :
                resultat = p.depiler() + p.depiler()
            else:
                resultat = p.depiler() * p.depiler()
            p.empiler(resultat)
    return p.depiler()
 ```



ligne1: création d'une pile p vide

ligne 2: parcours des elements de tab

ligne 3 : test si l'élement est un chiffre

ligne 4 :  l'élément est empiler sur p 

ligne 5 : sinon, on est ici dans le cas ou l'élément est un + ou un *

ligne 6 : test si l'élément est un +

ligne 7: affectation à resultat de la somme des 2 derniers éléments empilés

ligne 8 : sinon , on est ici dans la cas ou l'élément est un *

ligne 9 :  affectation à resultat du produit des 2 derniers éléments empilés

ligne 10: le resultat est empiler sur p 

ligne 11 : renvoi du dernier element empiler sur p qui est le resultat de l'opération 