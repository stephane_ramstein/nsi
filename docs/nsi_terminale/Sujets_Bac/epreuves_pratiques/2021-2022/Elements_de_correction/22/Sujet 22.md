# Sujet 22

## Exercice 1

Le but de cet exercice est le parcours inverse d'une chaine de caractère.

```python
def renverse(chaine):
    res=''
    for i in range(len(chaine)-1,-1,-1):
        res.append(chaine[i])
    return res
```

ligne 1: initialisation du résultat

ligne 2: parcours inverse de la chaine, i prendra les valeurs len(chaine)-1 puis len(chaine)-2 ect jusque 0

ligne 3: ajout de l'élément di'ndice i à res

ligne 4: renvoi du résultat

## Exercice 2

Le but de cet exercice est le parcours de tableau avec un pas différent de 1

```python
def crible(N):
    """renvoie un tableau contenant tous les nombres premiers plus petit que N"""
    premiers = []
    tab = [True] * N
    tab[0], tab[1] = False, False
    for i in range(2, N):
        if tab[i] == True:
            premiers.append(tab[i])
            for multiple in range(2*i, N, i):
                tab[multiple] = False
    return premiers

assert crible(40) == [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
```

ligne 1: initialisation de premier à un tableau vide

ligne 2: initialisation de tab à un tableau de longueur N de valeur True

ligne 3: passage à False des éléments d'indice 0 et 1 de tab 

ligne 4:  parcours des élémenrs du tableau à partir du 3 éme

ligne 5: test si l'élément d'indice i vaut True 

ligne 6: rajout du nombre i à premier

ligne 7 parcours des mutiples de i

ligne 8: passage à False des multiple de i

ligne 9: renvoi de premier 

