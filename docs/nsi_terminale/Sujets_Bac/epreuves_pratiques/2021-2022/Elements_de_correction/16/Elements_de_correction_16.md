# Elements de correction

## Exercice 1

Le but de cet exercice est d'utiliser les fonctions et méthodes python sur les listes afin de trouver l'élément maximum d'une liste ainsi que son indice.

Pour cet exercice 2 méthodes de résolution sont possibles:

- la première en utilisant les fonctions python
- la seconde en pacourant les éléments de la liste 

### 1ère méthode

```python
def maxi(l):
    valeur=max(l)
    indice=l.index(valeur)
    return (valeur,indice)
```

ligne 2: la fonction max renvoie la valeur maximale contenue dans la liste qui lui est passée en paramètre

ligne 3: La méthode index du module List permet de renvoyer l'indice de la première occurrence de l'élément passé en paramètre dans la liste sur laquelle elle agit

ligne 4: renvoie du couple (valeur,indice)

### 2ème méthode

```python
def maxi(l):
    indice=0
    valeur=l[0]
    for i in range(1,len(l)):
        if l[i]>valeur:
            valeur=l[i]
            indice=i
    return (valeur,indice)
```

Ici nous allons tout d'abord stocker le premier élément de la liste et son indice puis nous allons parcourir les éléments de la liste. Si on trouve un élément plus grand que celui déjà stocké alors on le stocke et on met à jour l'indice.

Ligne 2 et 3: stockage du premier élément de la liste et de son indice 

Ligne 4: parcours des  éléments de la liste excepté le premier

ligne 5: test si l'élément de la liste est plus grand que le stocké

ligne 6 et 7 : stockage du nouvel élément et de son indice 

Ligne 8 renvoie du couple (valeur,indice)

## Exercice 2

```python
def positif(T):
    T2 = list(T)
    T3 = []
    while T2 != []:
        x = T2.pop()
        if x >= 0:
            T3.append(x)
    T2 = []
    while T3 != []:
        x = T3.pop()
        T2.append(x)
    print('T = ',T)
    return T2
```

Le but de cet exercice est d'utiliser les méthodes pop et append,d'extraire certains éléments d'une pile sans la modifier, de parcourir une pile tout en conservant son ordre.

Pour cela dans un premier temps il faut créer une copie de la liste.

Puis dépiler les éléments de cette liste tout en stockant dans une seconde pile les positifs

Enfin, il faut remettre les éléments de la seconde pile dans l'ordre initial à l'aide d'une autre pile et renvoyer celle-ci.

Ligne 2: copie de T dans T2

Ligne 3: initialisation d'une pile vide

Ligne 4: parcours de T2 jusqu'à ce qu'elle soit vide

Ligne 5: dépiler T2 et stockage dans x

Ligne 6: test si l'élément est positif ou nul

Ligne 7: empilage de l'élément

Ligne 8: initialisation d'une pile vide

Ligne 9  à 11: remise dans l'ordre initial 

Ligne 12: affichage de la pile initiale

Ligne 13: renvoie de la pile ordonnée contenant les valeurs positives