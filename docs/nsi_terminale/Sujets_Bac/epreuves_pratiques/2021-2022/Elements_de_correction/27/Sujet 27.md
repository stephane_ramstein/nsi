# Sujet 27 

## Exercice 1

Le but de cet exercice est de travailler la récursivité ainsi que l'accés aux éléments d'un dictionnaire.

```python
def taille(arbre,lettre):
    fils=arbre[lettre]
    if fils==['','']:
        return 1
    elif fils[0]== '':
        return 1+ taille(arbre,fils[1])
    elif fils[1]== '':
        return 1+ taille(arbre,fils[0])
    else: 
        return 1+ taille(arbre,fils[0]) + taille(arbre,fils[0])
```

ligne 1: affectation à fils de la valeur associé à lettre dans le dictionnaire

ligne 2: test si la lettre n'a pas de fils

ligne 3: renvoi de 1

ligne 4: test si la lettre a uniquement un fils droit 

ligne 5: renvoi de 1 + la taille du sous arbre droit 

ligne 6: test si la lettre a uniquement un fils gauche 

ligne 7: renvoi de 1 + la taille du sous arbre gauche

ligne 8: sinon , on est donc dans le cas ou l'arbre a un fils gauche et un fils droit 

logne 9 renvoi de 1 + la taille du sous arbre gauche + la taille du sous arbre droit

## Exercice 2

Le but de cet exercice est de programmer une fonction de tri et d'utiliser les indices des éléments d'une liste

```python
def tri_iteratif(tab):
    for k in range( len(tab)-1, 0, -1):
        imax = k
        for i in range(0 , k):
            if tab[i] > tab[imax] :
                imax = i
        if tab[imax] > tab[k] :
            tab[k], tab[imax] = tab[imax] , tab[k]
    return tab

```

ligne 1: parcours des éléments du tableau en commençant par le dernier qui a donc l'indice len(tab)-1

ligne 2: affectation de imax au dernier élément de la liste non triée. Imax est l'indice de l'élément maximum dans la liste non triée

ligne 3: parcours des éléments de la liste non triée

ligne 4:  test si l'élément d'indice i est plus grand que l'élément d'indice imax

ligne 5: affectation de i à imax

ligne 6: test si l'élément d'indice imax est supérieur à l'élement d'indice k

ligne 7 : échange des éléments d'indice k et imax

ligne 8: renvoi du tableau 